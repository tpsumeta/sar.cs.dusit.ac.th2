<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Autdor     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<div class="panel default">
    <div class="panel-header text-center bg-lightBlue fg-white">
        ผลการดำเนินงาน 
    </div>

    <div class="panel-content">
        <s:iterator value="list" var="performance">

            <table class="table bordered">

                   <tr>
                    <td>ข้อ</td>
                    <td>${performance.pe_point}</td>
                </tr>
                <tr>
                    <td>เกณฑ์มาตรฐาน </td>
                    <td>${performance.p_standard}</td>
                </tr>

                <tr>
                    <td>ผลการดำเนินงาน</td>
                    <td>${performance.p_performance}</td>
                </tr>

                <tr>
                    <td>หลักฐานอ้างอิง</td>
                    <td>
                        <s:iterator value="listAtt" var="attList">
                            <a href="image/${attList.at_link}" target="_new">${attList.at_name}</a> 
                            <a href="AttachmentsDelete?at_id=${attList.at_id}" target="_blank" 
                               onclick="window.open('AttachmentsDelete?at_id=${attList.at_id}','name','width=600,height=400')">
                                <i class="icon  icon-cancel"></i></a>
                            <br/>
                        </s:iterator>

                        <a  class="button success" onclick="popupUploadForm()">เพิ่มไฟล์</a> 
                    </td>
                </tr>

            </table>

        </s:iterator>
        <div class="text-center">
            <a class="button primary" onclick="history.back();">กลับ</a>
            <a class="button  warning" href="PerformanceEdit?in_id=${in_id}&pe_id=${pe_id}">แก้ไข</a>
        </div>
    </div>
</div>


<script>
    var url = 'PerformanceDetail?in_id=${in_id}&pe_id=${pe_id}';
    function popupUploadForm() {
        var newWindow = window.open('AttachmentsForm?pe_id=${performance.pe_id}', 'name', 'height=500,width=600');
    }
</script>

