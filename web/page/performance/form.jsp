<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container" >
    ผลการดำเนินการ
    <s:form action="PerformanceSave" method="post">
        <s:hidden name="pe_id" label="รหัส"/>
        <%--<s:textfield  name="PerformanceModel.pe_have"  label="มี" type="number"/>--%>
        <s:radio list="# {'0': 'ไม่มี','1':'มี'}"
                 name="PerformanceModel.pe_have"/>
        <s:textfield  name="PerformanceModel.pe_point"  label="ข้อ" type="number"/>
        <s:textarea name="PerformanceModel.p_standard" label="เกณฑ์มาตรฐาน" cols="30" rows="10"/>
        <s:textarea name="PerformanceModel.p_performance" label="ผลการดำเนินงาน" cols="30" rows="10"/>
        <s:hidden name="PerformanceModel.in_id" value="%{in_id}" type="number"/>
        <s:submit value="บันทึก" cssClass="success"/>
    </s:form>
    


</div>

<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code  insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        content_css: "css/content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
</script>