<%-- 
    Document   : index
    Created on : 16 มิ.ย. 2557, 22:31:22
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../../conn.jsp" %>
<div class="">
    <div class="panel default">
        <div class="panel bg-lightBlue fg-white">
            <div class="grid fluid">
                <div class="row">
                    <div class="span12 text-center" style="font-size: 20px">&nbsp ประเมินคุณภาพ</div>
                </div>
            </div>
        </div>
        <div class="panel-content">
            <table class="table bordered">
                <tr>
                    <th>รายงานตัวบ่งชี้</th>
                </tr>
                <s:iterator value="YearList" var="el">
                    <tr>
                        <td>
                            <h4>องค์ประกอบที่ ${el.el_time}  ${el.el_name}  </h4>
                            <sql:query dataSource="${snapshot}" var="re">
                                SELECT * from sar_indicators
                                where el_id=${el.el_id} 
                                order by in_time asc
                            </sql:query>
                            <c:forEach var="row" items="${re.rows}">
                                <ul>ตัวบ่งชี้ที่${row.in_time}   ${row.in_name} 
                                    <c:choose>
                                        <c:when test="${row.in_time =='2.2'}">
                                            <a href="TwoDotTwoReport?in_id=${row.in_id}"><i class="icon-file-pdf"></i></a>
                                            </c:when>
                                            <c:when test="${row.in_time =='2.3'}">
                                            <a href="TwoDotThreeReport?in_id=${row.in_id}"><i class="icon-file-pdf"></i></a>
                                            </c:when>
                                            <c:when test="${row.in_time =='4.3'}">
                                            <a href="FourDotThreeReport?in_id=${row.in_id}"><i class="icon-file-pdf"></i></a>
                                            </c:when>
                                            <c:otherwise>
                                            <a href="ireport?in_id=${row.in_id}"><i class="icon-file-pdf"></i></a>
                                            </c:otherwise>
                                        </c:choose>
                                </ul>
                            </c:forEach>
                        </td>
                    </tr>
                </s:iterator>
            </table>
        </div>
    </div>

</div>