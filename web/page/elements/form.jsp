<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="ElementsSave" method="post">
        <s:hidden name="el_id" label="รหัส"/>
        <s:textfield  name="ElementsModel.el_time"  label="องค์ประกอบที่" type="number"/>
        <s:textfield name="ElementsModel.el_name" label="ชื่อองค์ประกอบ"/>
        <s:hidden name="ElementsModel.y_id" value="%{y_id}" type="number"/>
        <s:submit value="บันทึก" cssClass="success"/>
    </s:form>


</div>