<%-- 
    Document   : gen
    Created on : Oct 14, 2014, 10:20:51 PM
    Author     : dev
//
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@include file="../../../conn.jsp" %>

<h3>สร้างองค์ประกอบ</h3>
<button type="button" id="submit1" class="btn btn-default">สร้าง</button>
<sql:query dataSource="${snapshot}" var="re">
    SELECT * from sar_year  order by y_id asc
</sql:query>
<c:forEach var="year" items="${re.rows}">
    <ul>
        <input type="radio" name="sar_year" value="${year.y_name}"> ${year.y_name}
        <sql:query dataSource="${snapshot}" var="re">
            SELECT * from sar_elements  where y_id=${year.y_id}  order by el_time asc
        </sql:query>
        <c:forEach var="el" items="${re.rows}">
            <ul name="el" hidden="true">
                <input type="checkbox" name="sar_elements" value="${el.el_id}|${el.el_time}|${el.el_name}"> องค์ประกอบที่ ${el.el_time} ${el.el_name}
                <sql:query dataSource="${snapshot}" var="re">
                    SELECT * from sar_indicators where el_id=${el.el_id}  order by in_time asc
                </sql:query>
                <c:forEach var="in" items="${re.rows}">
                    <ul hidden="true" name="in">
                        <input type="checkbox" name="sar_indicators" value="${el.el_id}|${in.in_category}|${in.in_time}|${in.in_name}||${in_type}|${in_criteria}|${in_good}|${in_development}|${in_practices}|${in_target}|${in_num_performance}|${in_score}|${in_achieve}">ตัวบ่งชี้ที่ ${in.in_time} ${in.in_name}
                    </ul>
                </c:forEach>
            </ul>
        </c:forEach>
    </ul>
</c:forEach>

<form id="form1">
    <div id="form" size="100"></div>
    <s:hidden name="el_id" label="รหัส"/>
    <s:hidden  name="ElementsModel.el_time"  label="องค์ประกอบที่" type="number" id="el_time"/>
    <s:hidden name="ElementsModel.el_name" label="ชื่อองค์ประกอบ" id="el_name"/>
    <s:hidden name="ElementsModel.y_id" value="%{y_id}" type="number" />
</form>

<form id="form2">
    <s:textfield id="f2_in_id" name="in_id" label="รหัส" value="0"/>
    <s:textfield id="f2_in_category"  name="IndicatorsModel.in_category"  label="ประเภทตัวบ่งชี้" placeholder="สกอ สมส"/>
    <s:textfield id="f2_in_time"  name="IndicatorsModel.in_time"  label="ตัวบ่งชี้ที่" placeholder="ex 1.1"/>
    <s:textfield id="f2_in_name" name="IndicatorsModel.in_name" label="ชื่อ" placeholder="การบวนการแผนพัฒนา"/>
    <s:textfield id="f2_in_type" name="IndicatorsModel.in_type" label="ชนิดตัวบ่งชี้" placeholder="ชนิดตัวบ่งชี้"/>
    <s:textfield id="f2_in_criteria"  name="IndicatorsModel.in_criteria"  label="เกณฑ์การประเมิน" />
    <s:textfield id="f2_in_good" name="IndicatorsModel.in_good" label="จุดแข็ง" placeholder="จุดแข็ง"  />
    <s:textfield id="f2_in_development" name="IndicatorsModel.in_development" label="จุดที่ควรพัฒนา" placeholder="จุดที่ควรพัฒนา"  />
    <s:textfield id="f2_in_practices" name="IndicatorsModel.in_practices" label="วิธีปฏิบัติที่ดี" placeholder="วิธีปฏิบัติที่ดี" rows="5" />
    <s:textfield id="f2_in_target" name="IndicatorsModel.in_target"  label="ค่าเป้าหมาย" type="number"/>
    <s:textfield id="f2_in_num_performance" name="IndicatorsModel.in_num_performance"  label="ค่าเป้าหมาย" type="number"/>
    <s:textfield id="f2_in_score" name="IndicatorsModel.in_score"  label="ค่าเป้าหมาย" type="number"/>
    <s:textfield id="f2_in_achieve" name="IndicatorsModel.in_achieve"  label="ค่าเป้าหมาย" type="number"/>
    <s:textfield id="f2_el_id" name="IndicatorsModel.el_id"  label="รหัสองค์ประกอบ"/>
</form>

<script>
    $(document).ready(function () {
        console.log('ready');
        $("input[name=sar_year]").change(function () {
            console.log('checked');
            // if(this.checked) {
            if ($(this).is(':checked')) {
                $(this).parent().find('ul[name=el]').show('fast');
                $("input").not(this).parent().find('ul').hide("fast");
                console.log('ul');
                $(this).parent().find(':checkbox').attr('checked', true);
                $("input").not(this).parent().find(':checkbox').attr("checked", false);
            } else {
                $(this).parent().find('ul[name=el]').hide('fast');
            }
        });

        $("input[name=sar_elements]").click(function () {
            if (this.checked) {
                $(this).parent().find('ul[name=in]').show('fast');

            } else {
                $(this).parent().find('ul[name=in]').hide('fast');
            }
        });


        $('#submit1').click(function (event) {
            fields = $("input[name=sar_elements").serializeArray();
            $.each(fields, function (i, field) {
                // $("#form").append('<input name="sar_elements[]" value="'+field.value+'"/> ');
                console.log(field.value);
                var str = field.value;
                var res = str.split("|");
                console.log(res[0]);
                $("#el_time").val(res[1]);
                $("#el_name").val(res[2]);
                $.ajax({
                    url: 'ElementsSaveJson',
                    type: 'post',
                    data: $("#form1").serializeArray(),
                    success: function (data) {
                        console.log('insert el ok');
                        // insert indicator
                        indicator = $("input[name=sar_indicators").serializeArray();
                        $.each(indicator, function (i, indi) {
                            var str2 = indi.value;
                            var res2 = str2.split("|");
                            if (res[0] == res2[0]) {
                               $("#f2_in_category").val(res2[1]);
                               $("#f2_in_time").val(res2[2]);
                               $("#f2_in_name").val(res2[3]);
                               $("#f2_in_type").val(res2[4]);
                               $("#f2_in_criteria").val(res2[5]);
                               $("#f2_in_good").val(res2[6]);
                               $("#f2_in_development").val(res2[7]);
                               $("#f2_in_practices").val(res2[8]);
                               $("#f2_in_target").val(res2[9]);
                               $("#f2_in_num_performance").val(res2[10]);
                               $("#f2_in_score").val(res2[11]);
                               $("#f2_in_achieve").val(res2[12]);
                               $("#f2_el_id").val(data);

                            }
                        });
                    },
                    error: function () {
                        console.log("ajax error")
                    }
                });
            });

        });

    });
</script>