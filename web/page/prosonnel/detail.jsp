<%-- 
    Document   : prosonnel/detail
    Created on : 16 มิ.ย. 2557, 16:03:16
    Author     :  Top_Suemta <tpsumeta@gmail.com>
    หน้าแสดงรายละเอียดของบุคลกร
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<div class="panel default">
    <div class="panel-header text-center">
        ข้อมูลบุคลากร       
        <div class="text-right"><a class="button success" href="ProsonnalForm"><span class="icon-plus"></span> เพิ่มบุคลากร</a></div>
    </div>

    <div class="panel-content">

        <table class="table bordered">
            <s:iterator value="prosonnelList" var="prosonnel">
                <tr>
                    <td colspan="2" class="text-center">
                        <img src="image/${prosonnel.p_img}" width="200">
                    </td>
                </tr>
                <tr>
                    <td>ชื่อ</td>
                    <td>${prosonnel.p_name}</td>
                </tr>
                <tr>
                    <td>เบอร์โทร</td>
                    <td>${prosonnel.p_tel}</td>
                </tr>
                <tr>
                    <td>อีเมล์</td>
                    <td>${prosonnel.p_email}</td>
                </tr>
                <tr>
                <td>รหัสบัตรประชาชน</td>
                <td>${prosonnel.p_idcard}</td>
                </tr>
            </s:iterator>

        </table>
    </div>
</div>