<%-- 
    Document   : prosonnel/index
    Created on : 13 มิ.ย. 2557, 19:22:14
    Author     : Top_Sumeta
    แสดงรายชื่อบุคลาการ
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<div class="panel default">
    <div class="panel-header text-center bg-lightBlue fg-white">
        ข้อมูลบุคลากร       
        <div class="text-right"><a class="button success" href="ProsonnalForm"><span class="icon-plus"></span> เพิ่มบุคลากร</a></div>
    </div>

    <div class="panel-content">
        <div class="text-left">
            <a class="button success" href="ProsonnalIndex?p_active=9"><span class="icon-list"></span> ทั้งหมด</a>
            <a class="button success" href="ProsonnalIndex?p_active=1"><span class="icon-checkmark"></span> ใช้งานอยุ่</a>
            <a class="button success" href="ProsonnalIndex?p_active=0"><span class="icon-cancel-2"></span> ปิดใช้งาน</a>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <td>รูป</td>
                    <td>ชื่อ</td>

                    <td>ตำแหน่ง</td>
                    <td>สถานะ</td>
                    <td>จัดการ</td>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="prosonnelList" var="prosonnel">
                    <tr>
                        <td>
                            <img src="image/${prosonnel.p_img}" width="50">
                        </td>
                        <td><a href="ProsonnalWhere?p_id=${prosonnel.p_id}">${prosonnel.p_name}</a></td>
                   
                        <td>${prosonnel.p_status}</td>
                        <td>
                            <s:if test="%{#prosonnel.p_active==0}">ปิดใช้งาน</s:if>
                            <s:if test="%{#prosonnel.p_active==1}">เปิดใช้งาน</s:if>
                            </td>
                            <td>
                                <a href="ProsonnalEdit?p_id=${prosonnel.p_id}"><i class="icon-pencil"></i></a>
                            <s:if test="%{#prosonnel.p_status != 'ผู้ดูแลระบบ'}" >
                            <a href="ProsonnalDelete?p_id=${prosonnel.p_id}"><i class="icon-cancel-2"></i></a>
                            </s:if>
                        </td>
                    </tr>
                </s:iterator>
            </tbody>
        </table>



    </div>
</div>


