<%-- 
    Document   : index
    Created on : 5 ก.ค. 2557, 23:22:16
    Author     : dev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<div class="container">
    <div class="panel default">
        <div class="panel bg-lightBlue fg-white">

            <div class="grid fluid">
                <div class="row">
                    <div class="span9" style="font-size: 20px">&nbsp ไฟล์แนบ</div>
                    <div class="span3 text-right"><a href="AttachmentsForm" class="button primary"> + เพิ่มไฟล์แนบ</a>&nbsp</div>
                </div>
            </div>

        </div>
        <div class="panel-content">

            <table class="table bordered">

                <tr><th>ชื่อ</th><th>ลิงค์</th><th>ชื่อผู้สอน</th><th>จัดการ</th></tr>
                        <s:iterator value="attList" var="a">
                    <tr>
                        <td><a href="image/${a.at_link}">${at_name}</a></td>

                        <td><a href="AttachmentsDetail?a_id=${a.at_id}">${a.at_link}</a></td>
                        <td>        

                            <s:action name="ProsonnalWhere" executeResult="true">
                                <s:param name="switch" value="2">true</s:param>
                            </s:action>

                        </td>
                        <td><a href="AttachmentsDelete?at_id=${a.at_id}">ลบ</a></td>
                    </tr>
                </s:iterator>
            </table>




        </div>
    </div>

</div>

