<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="YearSave" method="post" id="form1" >
        <s:textfield name="yearModel.y_name" label="ปี"/>
        <%--<s:textfield name="yearModel.y_time" label="ครั้งที่"/>--%>
        <s:hidden name="y_id"/>
        <s:submit value="บันทึก" cssClass="info"/>
    </s:form>

</div>

<script>
    $("#form1").submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "YearSave",
            data: $(this).serialize(),
            success: function() {
                alert("บันทึกข้อมูลเรียบร้อย");
                window.opener.location.reload(false);
                self.close();
            }
        });
    });
</script>
