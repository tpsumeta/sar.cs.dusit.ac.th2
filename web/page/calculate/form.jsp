<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="CalculateSave" method="post" id="form1">
        <s:hidden name="ca_id" label="รหัส"/>
        <s:hidden name="CalculateModel.ca_name" label="ชื่อ" id='name'/>
        <s:textfield name="CalculateModel.ca_value" label="ค่า"/>
        <s:textfield name="CalculateModel.ca_comment" label="หมายเหตุ" id="comment"/>
        <s:hidden name="CalculateModel.in_id" label="in_id" id="in_id"/>
        <s:hidden name="CalculateModel.no" label="ลำดับ" id="no"/>
        <s:hidden name="CalculateModel.ca_link" label="ลิงค์" id="ca_link"/>
        <s:submit value="บันทึก"/>
    </s:form>


</div>


<script type="text/javascript">

    $("#form1").submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "CalculateSave",
            data: $(this).serialize(),
            success: function() {
                window.opener.location.reload(false);
                self.close();
            }
        });
    });

</script>