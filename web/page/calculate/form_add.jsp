<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="CalculateSave" method="post" onsubmit="return closeSelf()">
        <s:hidden name="ca_id" label="รหัส"/>
        <s:hidden name="CalculateModel.ca_name" label="ชื่อ" id='name'/>
        <s:textfield name="CalculateModel.ca_value" label="ค่า"/>
        <s:textfield name="CalculateModel.ca_comment" label="หมายเหตุ" id="comment"/>
        <s:hidden name="CalculateModel.in_id" label="in_id" id="in_id"/>
        <s:hidden name="CalculateModel.no" label="ลำดับ" id="no"/>
        <s:hidden name="CalculateModel.ca_link" label="ลิงค์" id="ca_link"/>

        <s:submit value="บันทึก"/>
    </s:form>


</div>


<script type="text/javascript">
    function closeSelf() {

        window.opener.location.reload(false);
        self.close();
        return true;
    }


    function GetURLParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }

    var ca_name = GetURLParameter('ca_name');
    var in_id = GetURLParameter('in_id');
    var no = GetURLParameter('no');


    $('#name').val(ca_name);
    $('#in_id').val(in_id);
    $('#no').val(no);




</script>