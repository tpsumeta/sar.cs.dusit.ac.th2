<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@include file="../../conn.jsp" %>
<s:form  id="fm">
    <s:hidden name="ip_id" label="รหัส"/>
    <s:hidden  name="ReIndicatorsPersonnelModel.in_id"  label="ตัวบ่งชี้" type="number" value="%{in_id}" readonly="true"/>
    <sql:query dataSource="${snapshot}" var="p1">
        SELECT * from personnel
    </sql:query>
    <select name="ReIndicatorsPersonnelModel.p_id">
        <c:forEach var="row" items="${p1.rows}">
            <option value="${row.p_id}">${row.p_name}</option>
        </c:forEach>
    </select>
    <s:hidden name="ReIndicatorsPersonnelModel.ip_position" label="ตำแหน่งบุคลากร" value="%{ip_position}" readonly="true"/>
    <s:submit value="บันทึก"/>
</s:form>


<script type="text/javascript">
 
    $("#fm").submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "ReIndicatorsPersonnelSave",
            data: $(this).serialize(),
            success: function() {
                console.log("Save OK");
                window.opener.location.reload(false);
                self.close();
            }
        });
    });

</script>