<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Autdor     : Top_Sumeta
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@include file="../../conn.jsp" %>

<sql:query dataSource="${snapshot}" var="att">
    SELECT * from re_indicators_personnel
    left join personnel
    on re_indicators_personnel.p_id = personnel.p_id
    where in_id=${in_id}  and personnel.p_id=${loginID} and ip_position =0
</sql:query>
<c:if test="${att.rowCount < 1 }">
    <c:if test="${status != 'ผู้ดูแลระบบ' }">
        <script>
            alert("${loginUser} คุณไม่มีสิทธิถึงใช้หน้านี้");
            history.back()
        </script>
    </c:if>
</c:if> 

<div class="panel default">
    <div class="panel-header text-center bg-lightBlue fg-white">
        ตัวบ่งชี้   
    </div>

    <div class="panel-content">
        <a href="ireport?in_id=${in_id}" target="_new"><button class="info"><i class="icon-printer"></i> รายงาน</button></a>   
        <a href="IndicatorsEdit?in_id=${in_id}" target="_new"><button class="warning"><i class="icon-pencil"></i> แก้ไข</button></a>   
        <br/><br/>
        <table class="table bordered">
            <s:iterator value="list" var="indicators">
                <tr><td width="10%">ตัวบ่งชี้</td><td>${indicators.in_category}</td></tr>
                <tr><td>ที่</td><td>${indicators.in_time}</td></tr>
                <tr><td>ชื่อ</td><td>${indicators.in_name}</td></tr>
            </s:iterator>
        </table>

        <b>เกณฑ์การให้คะแนน </b>
        <a href="#" onclick="Cri(${in_id})" class="button mini info"><i class="icon icon-pencil"></i></a>
        <table class="table bordered">
            <tr class="text-center  text-bold">
                <s:iterator value="CrList" var="crl">
                    <td>${crl.cr_score} คะแนน</td>
                </s:iterator>
            </tr>
            <tr class="text-center">
                <s:iterator value="CrList" var="crl">
                    <td>${crl.cr_text}</td>
                </s:iterator>
            </tr>
        </table>

        <br/>
        <table class="table bordered">
            <tr>
                <th colspan="3" style="width: 85%"  class="panel-header text-center bg-lightBlue fg-white">ผลการดำเนินงาน</th>  
                <th class="panel-header text-center bg-lightBlue fg-white">
                    <a href="PerformanceForm?in_id=${in_id}" class="button success" ><i class="icon-plus-2"></i> เพิ่ม </a>
                </th> 
            </tr>

            <tr><th>ชื่อ</th><th>มี</th><th>เกณฑ์มาตรฐาน</th><th>จัดการ</th></tr>
                    <s:set value="0" var="sum" />
                    <s:set value="0" var="sum_have" />
                    <s:iterator value="performanceList" var="performance">
                <tr>
                    <td>${performance.pe_point}</td>
                    <td>
                        <s:if test="%{pe_have==0}">
                            ไม่มี
                        </s:if>
                        <s:else>
                            มี <s:set value="%{#sum+1}" var="sum"  />
                        </s:else>
                    </td>
                    <s:set value="%{#sum_have+1}" var="sum_have"  />
                    <td><a href="PerformanceDetail?in_id=${performance.in_id}&pe_id=${performance.pe_id}">${performance.p_standard}</a></td>
                    <td class="text-center">
                        <a class="button  warning" href="PerformanceEdit?in_id=${performance.in_id}&pe_id=${performance.pe_id}"><i class="icon-pencil"></i> แก้ไข</a> 
                        <a class="button  danger" href="PerformanceDelete?in_id=${performance.in_id}&pe_id=${performance.pe_id}"><i class="icon-remove"></i> ลบ</a>
                    </td>
                </tr>
            </s:iterator>

        </table>

        <table class="table bordered">
            <tr>
                <th colspan="4" class="panel-header text-center bg-lightBlue fg-white">
                    การประเมินตนเอง   
                </th>
            </tr>
            <tr>
                <th>เป้าหมาย</th>
                <th>ผลการดำเนินงาน</th>
                <th>คะแนนการประเมินตนเอง</th>
                <th>การบรรลุเป้าหมาย</th>
            </tr>
            <tr>
                <td>${indicators.in_target} ข้อ             
                </td><td>${sum} ข้อ   <s:hidden value="%{#sum}"  id="sum_per" /></td>
                <td>${indicators.in_score}  คะแนน  <s:hidden value="5"  id="score" /></td>
                <td>
                    <s:if test="%{#indicators.in_achieve==0}">
                        ไม่บรรลุ <s:hidden value="0"  id="ach" />
                    </s:if>
                    <s:else>
                        บรรลุ<s:hidden value="1"  id="ach" />
                    </s:else>
                </td>
            </tr>
        </table>

        <b> จุดแข็ง/แนวทางเสริมจุดแข็ง</b> <br/>
        ${indicators.in_good}<br/><br/>
        <b>จุดที่ควรพัฒนา/ข้อเสนอแนะในการปรับปรุง</b><br/>
        ${indicators.in_development}<br/><br/>
        <b>วิธีปฏิบัติที่ดี/นวัตกรรม (ถ้ามี)</b><br/>
        ${indicators.in_practices}
        <br/><br/>



        <sql:query dataSource="${snapshot}" var="re">
            SELECT * from re_indicators_personnel
            left join personnel
            on re_indicators_personnel.p_id = personnel.p_id
            where in_id=${in_id} 
        </sql:query>
        <hr/>
        <h4>ผู้กำกับ <a class="mini info"  onclick="pop(1)"><i class="icon-plus-2"></i></a></h4>
                <c:forEach var="row" items="${re.rows}">
                    <c:if test="${row.ip_position == 1}">
                        ${row.p_name} <a name="del1" value="${row.ip_id}"><i class="icon-remove fg-red" ></i></a><br/>
                </c:if>
            </c:forEach>

        <h4>ผู้จัดเก็บข้อมูล  <a class="mini info"  onclick="pop(2)"><i class="icon-plus-2"></i></a></h4> 
                <c:forEach var="row" items="${re.rows}">
                    <c:if test="${row.ip_position == 2}">
                        ${row.p_name} <a name="del1"  value="${row.ip_id}"><i class="icon-remove fg-red" ></i></a><br/>
                </c:if>
            </c:forEach>

        <h4>ผู้เขียนผลการดำเนินการ  <a class="mini info"  onclick="pop(3)"><i class="icon-plus-2"></i></a></h4> 
                <c:forEach var="row" items="${re.rows}">
                    <c:if test="${row.ip_position == 3}">
                        ${row.p_name} <a name="del1" value="${row.ip_id}"><i class="icon-remove fg-red" ></i></a><br/>
                </c:if>
            </c:forEach>
            <c:if test="${status == 'ผู้ดูแลระบบ' }">
            <hr/>
            <h4>ผู้มีสิทธิเข้าใช้  <a class="mini info"  onclick="pop(0)"><i class="icon-plus-2"></i></a></h4> 
                    <c:forEach var="row" items="${re.rows}">
                        <c:if test="${row.ip_position == 0}">
                            ${row.p_name} <a name="del1" value="${row.ip_id}"><i class="icon-remove fg-red" ></i></a><br/>
                    </c:if>
                </c:forEach>
            </c:if>

        <center>
            <s:form action="IndicatorsSave" method="post" id="form1">
                <s:hidden name="in_id" label="รหัส"/>     
                <s:hidden name="IndicatorsModel.in_num_performance" id="per" label="ผลการดำเนินงาน"/>
                <s:hidden name="IndicatorsModel.in_score" id="in_score"  label="คะแนน"/>
                <s:hidden name="IndicatorsModel.in_achieve"  id="in_achieve" label="บรรลุ"/>
                <s:hidden  name="IndicatorsModel.in_category"  label="หมวดหมู่" value="%{#indicators.in_category}"/>
                <s:hidden  name="IndicatorsModel.in_time"  label="ตัวบ่งชี้ที่" value="%{#indicators.in_time}"/>
                <s:hidden  name="IndicatorsModel.in_name"  label="ชื่อ" value="%{#indicators.in_name}"/>
                <s:hidden  name="IndicatorsModel.in_type"  label="ชนิด" value="%{#indicators.in_type}"/>
                <s:hidden  name="IndicatorsModel.in_good"  label="จุดแข็ง" value="%{#indicators.in_good}"/>
                <s:hidden  name="IndicatorsModel.in_development"  label="จุดแข็ง" value="%{#indicators.in_development}"/>
                <s:hidden  name="IndicatorsModel.in_practices"  label="จุดแข็ง" value="%{#indicators.in_practices}"/>
                <s:hidden  name="IndicatorsModel.el_id" value="%{#indicators.el_id}"/>
                <s:hidden  name="IndicatorsModel.in_target"   value="%{#indicators.in_target}"  id="target"  label="เป้าหมาย"/>
                <s:hidden  id="sum_have"  value="%{#sum_have}" />
                <%--<s:submit value="บันทึก" cssClass="btn success"/>--%>
            </s:form>
        </center>

        <s:iterator value="ScoreList" var="score">
            <s:if test="%{#score.performance ==#sum}">
                <input id="s1" value="${score.cr_score}" type="hidden"/>
            </s:if>
        </s:iterator>
    </div>
</div>

<script>
    $(document).ready(function () {
//    คำนวณ ScoreList
        $("#in_score").val($("#s1").val());

//    ผลการดำเนินงาน
        $("#per").val(${sum});

//    การบรรลุเป้าหมาย
        if ($("#sum_per").val() >= $("#target").val()) {
            $("#in_achieve").val(1);
        } else {
            $("#in_achieve").val(0);
        }
    });

    // ลบตำแหน่ง
    $("a[name='del1']").on('click', function () {
        $.get("ReIndicatorsPersonnelDelete?ip_id=" + $(this).attr("value"), function (data) {
            alert("ลบข้อมูลสำเร็จ");
            window.location.reload();
        });
    });

    // บันทึกข้อมูล
    $("a").mouseover(function () {
        $("#form1").submit();
    });
    $("#form1").submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "IndicatorsSave",
            data: $(this).serialize(),
            success: function () {
                console.log("SAVA OK!!");
            }
        });
    });

//    เกณฑ์การให้คะแนน
    function Cri(in_id) {
        var newWindow = window.open('CriteriaIndex?in_id=' + in_id, 'name', "left=100, top=100,height=400,width=600");
    }

// เพิ่มตำแหน่งบุคลาการ
    function pop(ip_position) {
        var newWindow = window.open('ReIndicatorsPersonnelForm?ip_position=' + ip_position + '&in_id=' +${in_id},
                'name',
                "top=100,left=100, height=400,width=600");
    }

// ลบบุคลาการ
    function del() {
        var newWindow = window.open('ReIndicatorsPersonnelForm?ip_position=3&in_id=' +${in_id}, 'name', "height=200,width=300");
    }

</script>



