<%-- 
    Document   : index
    Created on : 8 ส.ค. 2557, 10:20:27
    Author     : dev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@include file="../../../conn.jsp" %>

<sql:query dataSource="${snapshot}" var="att">
    SELECT * from re_indicators_personnel
    left join personnel
    on re_indicators_personnel.p_id = personnel.p_id
    where in_id=${in_id}  and personnel.p_id=${loginID} and ip_position =0
</sql:query>
<c:if test="${att.rowCount < 1 }">
    <c:if test="${status != 'ผู้ดูแลระบบ' }">
        <script>
            alert("${loginUser} คุณไม่มีสิทธิถึงใช้หน้านี้");
            history.back()
        </script>
    </c:if>
</c:if> 

<div class="panel default">
    <div class="panel-header text-center bg-lightBlue fg-white">
        ตัวบ่งชี้
    </div>

    <div class="panel-content">
        <a onclick="fnsave();"><button class="success"><i class="icon-sale"></i> บันทึก</button></a>   
        <a href="TwoDotTwoReport?in_id=${in_id}" target="_blank"><button class="info"><i class="icon-printer"></i> รายงาน</button></a>   
        <a href="TwoDotTwoEdit?in_id=${in_id}" target="_blank"><button class="warning"><i class="icon-pencil"></i> แก้ไข</button></a>   
        <br/><br/>

        <s:iterator value="list" var="in">
            <table class="table bordered">
                <tr><td>ตัวบ่งชี้ที่ ${in.in_time}</td><td>${in.in_name}</td></tr>
                <tr><td>ชนิดของตัวบ่งชี้</td><td>${in.in_type}</td></tr>
                <tr><td>เกณฑ์การประเมิน</td><td>${in.in_criteria}</td></tr>
                <tr> <td>ผลดำเนินงาน</td><td></td></tr>
                <tr><td>จำนวนอาจารย์ </td>
                    <td>
                        <table class="border" style="width:100%">
                            <tr>
                                <th rowspan="2">ลำด้บ</th>
                                <th rowspan="2">ปีการศึกษา</th>
                                <th colspan="4">จำนวนอาจารย์</th>
                                <th rowspan="2" >หลักฐาน</th>
                            </tr>
                            <tr>  
                                <th>ป.ตรี</th>
                                <th>ป.โท</th>
                                <th>ป.เอก</th>
                                <th>รวม</th>
                            </tr>

                            <tr>               
                                <td>
                                    1
                                </td>      

                                <td>
                                    <s:iterator value="CalList" var="Cal">
                                        <c:if test="${Cal.ca_name == 'year'}">
                                            <c:set value="1" var="year1"/>
                                            <a  id="year1" onclick="cal(${Cal.ca_id});">${Cal.ca_value}</a>
                                        </c:if>  
                                    </s:iterator>
                                    <c:if test="${year1 !=1}">
                                        <a onclick="addcal(0, 'year', 'ปี', 1);"><i class="icon-plus-2"></i></a>
                                        </c:if>      
                                </td>   
                                <td>
                                    <s:iterator value="CalList" var="Cal">
                                        <c:if test="${Cal.ca_name == 'ba'}">
                                            <c:set value="1" var="ba1"/>
                                            <a  id="ba1" onclick="cal(${Cal.ca_id});">${Cal.ca_value} </a>
                                        </c:if>
                                    </s:iterator>
                                    <c:if test="${ba1 !=1}">
                                        <a onclick="addcal(0, 'ba', '', 1);"><i class="icon-plus-2"></i></a>
                                        </c:if>    
                                </td>   
                                <td>
                                    <s:iterator value="CalList" var="Cal">
                                        <c:if test="${Cal.ca_name == 'mba'}">
                                            <c:set value="1" var="mba1"/>
                                            <a id="mba1" onclick="cal(${Cal.ca_id});">${Cal.ca_value}</a>
                                        </c:if>
                                    </s:iterator>
                                    <c:if test="${mba1 !=1}">
                                        <a onclick="addcal(0, 'mba', '', 1);"><i class="icon-plus-2"></i></a>
                                        </c:if>    
                                </td>  
                                <td>
                                    <s:iterator value="CalList" var="Cal">
                                        <c:if test="${Cal.ca_name == 'phd'}">
                                            <c:set value="1" var="phd1"/>
                                            <a  id="phd1" onclick="cal(${Cal.ca_id});"> ${Cal.ca_value} </a>
                                        </c:if>
                                    </s:iterator>
                                    <c:if test="${phd1 !=1}">
                                        <a onclick="addcal(0, 'phd', '', 1);"><i class="icon-plus-2"></i></a>
                                        </c:if>   
                                </td>  
                                <td><lable  id="sum1" onclick="cal(${Cal.ca_id});">${Cal.ca_value}</lable></td>  
                    <td>
                        <a onclick="AddAtt(1)"><i class="icon-plus-2"></i></a>
                            <s:iterator value="CalList" var="Cal">
                                <c:if test="${Cal.ca_name == 'file'}">
                                <a href="image/${Cal.ca_link}"> ${Cal.ca_comment}</a>
                                <a onclick="DelAtt(${Cal.ca_id});"><i class="icon-minus-2"></i></a><br/>
                                </c:if>
                            </s:iterator>

                    </td>
                </tr>
                <tr><td>2</td>      
                    <td>
                        <s:iterator value="CalList2" var="Cal">
                            <c:if test="${Cal.ca_name == 'year'}">
                                <a  id="year2" onclick="cal(${Cal.ca_id});">${Cal.ca_value} </a>
                                <c:set value="1" var="year2"/>
                            </c:if>
                        </s:iterator>
                        <c:if test="${year2 !=1}">
                            <a onclick="addcal(0, 'year', 'ปี', 2);"><i class="icon-plus-2"></i></a>
                            </c:if>      
                    </td>   
                    <td>
                        <s:iterator value="CalList2" var="Cal">
                            <c:if test="${Cal.ca_name == 'ba'}">
                                <c:set value="1" var="ba2"/>
                                <a id="ba2" onclick="cal(${Cal.ca_id});">${Cal.ca_value} </a>
                            </c:if>
                        </s:iterator>
                        <c:if test="${ba2 !=1}">
                            <a onclick="addcal(0, 'ba', '', 2);"><i class="icon-plus-2"></i></a>
                            </c:if>    
                    </td>   
                    <td>
                        <s:iterator value="CalList2" var="Cal">
                            <c:if test="${Cal.ca_name == 'mba'}">
                                <c:set value="1" var="mba2"/>
                                <a id="mba2" onclick="cal(${Cal.ca_id});">${Cal.ca_value}</a>
                            </c:if>
                        </s:iterator>
                        <c:if test="${mba2 !=1}">
                            <a onclick="addcal(0, 'mba', '', 2);"><i class="icon-plus-2"></i></a>
                            </c:if>
                    </td>  
                    <td>
                        <s:iterator value="CalList2" var="Cal">
                            <c:if test="${Cal.ca_name == 'phd'}">
                                <c:set value="1" var="phd2"/>
                                <a  id="phd2" onclick="cal(${Cal.ca_id});">${Cal.ca_value} </a>
                            </c:if>
                        </s:iterator>
                        <c:if test="${phd2 !=1}">
                            <a onclick="addcal(0, 'phd', '', 2);"><i class="icon-plus-2"></i></a>
                            </c:if>  
                    </td>  
                    <td>
                        <s:iterator value="CalList2" var="Cal">
                            <c:if test="${Cal.ca_name == 'sum'}">
                        <lable id="sum2"> </lable>
                        </c:if>
                    </s:iterator>
                </td>  
                <td>
                    <a onclick="AddAtt(2);"><i class="icon-plus-2"></i></a>
                        <s:iterator value="CalList2" var="Cal">
                            <c:if test="${Cal.ca_name == 'file'}">
                            <a href="image/${Cal.ca_link}"> ${Cal.ca_comment}</a>
                            <a onclick="DelAtt(${Cal.ca_id});"><i class="icon-minus-2"></i></a><br/>
                            </c:if>
                        </s:iterator>
                </td>
                </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td>ผลการคำนวณ</td>
                <td>
                    <table class="border" style="width:100%">
                        <tr>
                            <th>ลำดับ</th>
                            <th>ปีการศึกษา</th>
                            <th>อาจารย์ทั้งหมด</th>
                            <th>อาจารย์ ป.เอก</th>
                            <th>อาจารย์ ค่าร้อยละ</th>
                        </tr>
                        <tr><td>1</td>
                            <td> <label id="gyear1"></label></td>
                            <td> <label id="gsum1"></label></td>
                            <td> <label id="gphd1"></label></td>
                            <td> <label id="percent1"></label></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td> <label id="gyear2"></label></td>
                            <td> <label id="gsum2"></label></td>
                            <td> <label id="gphd2"></label></td>
                            <td> <label id="percent2"></label></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td colspan="3">ค่าร้อยละที่เพิ่มขึ้น</td>
                            <td> <label id="padd"></label></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td colspan="3">ค่าคะแนนที่ได้ จากการคำนวณตามแบบที่ 1</td>
                            <td><label id="score1"></label></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td colspan="3">ค่าคะแนนที่ได้ จากการคำนวณตามแบบที่ 2</td>
                            <td><label id="score2"></label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>เอกสารหลักฐานอ้างอิง</td>
                <td></td>
            </tr>
            <tr>
                <td>การประเมินตนเอง</td>
                <td>
                    <table class="border" style="width:100%"> 
                        <tr>
                            <th>เป้าหมาย</th>
                            <th>ผลการดำเนินงาน</th>
                            <th>คะแนนประเมินตนเอง</th>
                            <th>การบรรลุเป้าหมาย</th>
                        </tr>
                        <tr>
                            <th>ร้อยละ ${in.in_target} ขึ้นไป</th>
                            <th>ร้อยละ <b id="num_performance"></b></th>
                            <th><b id="in_score"><b/> คะแนน</th>
                            <th><b id="achieve">${in.in_achieve}<b/></th>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td>จุดแข็ง/แนวทางเสริมจุดแข็ง</td>
                <td>${in.in_good}</td>
            </tr>
            <tr>
                <td>จุดควรพัฒนา/ข้อเสนอแนะในการปรับปรุง</td>
                <td>${in.in_development}</td>
            </tr>
            <tr>
                <td>วิธิปฏิบัติที่ดี/นวัตกรรม</td>
                <td>${in.in_practices}</td>
            </tr>
            <sql:query dataSource="${snapshot}" var="re">
                SELECT * from re_indicators_personnel
                left join personnel
                on re_indicators_personnel.p_id = personnel.p_id
                where in_id=${in_id} 
            </sql:query>
            <tr>
                <td>ผู้กำกับ <a  onclick="pop(1)">+</a></td>
                <td> 
                    <c:forEach var="row" items="${re.rows}">
                        <c:if test="${row.ip_position == 1}">
                            ${row.p_name} <a href="ReIndicatorsPersonnelDelete?ip_id=${row.ip_id}" target="_new">-</a><br/>
                        </c:if>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td>ผู้จัดเก็บข้อมูล <a  onclick="pop(2)">+</a></td>
                <td>    
                    <c:forEach var="row" items="${re.rows}">
                        <c:if test="${row.ip_position == 2}">
                            ${row.p_name} <a href="ReIndicatorsPersonnelDelete?ip_id=${row.ip_id}" target="_new">-</a><br/>
                        </c:if>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td>ผู้เขียนผลการดำเนินการ  <a  onclick="pop(3)">+</a></td>
                <td>      
                    <c:forEach var="row" items="${re.rows}">
                        <c:if test="${row.ip_position == 3}">
                            ${row.p_name} <a href="ReIndicatorsPersonnelDelete?ip_id=${row.ip_id}" target="_new">-</a><br/>
                        </c:if>
                    </c:forEach>
                </td>
            </tr>

            </table>
            
               <c:if test="${status == 'ผู้ดูแลระบบ' }">
            <hr/>
            <h4>ผู้มีสิทธิเข้าใช้  <a class="mini info"  onclick="pop(0)"><i class="icon-plus-2"></i></a></h4> 
                    <c:forEach var="row" items="${re.rows}">
                        <c:if test="${row.ip_position == 0}">
                            ${row.p_name} <a name="del1" value="${row.ip_id}"><i class="icon-remove fg-red" ></i></a><br/>
                    </c:if>
                </c:forEach>
            </c:if>

        </s:iterator>

        <div class=" offset6">
            <s:form action="TwoDotTwoSave" method="post"  id="form1" target="_blank">
                <s:hidden name="in_id" label="รหัส"/>
                <s:hidden  name="IndicatorsModel.in_category"  label="หมวดหมู่"/>
                <s:hidden  name="IndicatorsModel.in_time"  label="ตัวบ่งชี้ที่"/>
                <s:hidden  name="IndicatorsModel.in_name"  label="ชื่อ"/>
                <s:hidden  name="IndicatorsModel.in_type"  label="ชนิด"/>
                <s:hidden  name="IndicatorsModel.in_criteria"  label="เกณฑ์การประเมิน"/>
                <s:hidden  name="IndicatorsModel.in_good"  label="จุดแข็ง"/>
                <s:hidden  name="IndicatorsModel.in_development"  label="จุดควรพัฒนา"/>
                <s:hidden  name="IndicatorsModel.in_practices"  label="แนวทางปฏัติที่ดี"/>
                <s:hidden  name="IndicatorsModel.el_id"/>
                <s:hidden  name="IndicatorsModel.in_target"  label="เป้าหมาย" id="target"/>
                <s:hidden  name="IndicatorsModel.in_numerator"  label="อาจารย์ที่วุฒิปริญาเอก" type="number" id="in_num" />
                <s:hidden  name="IndicatorsModel.in_divisor"  label="อาจารย์ทั้งหมด" type="number" id="in_div" />
                <s:hidden  name="IndicatorsModel.in_num_performance"  id="per"  readonly="true" label="ค่าร้อยละ"/>
                <s:hidden  name="IndicatorsModel.in_num_performance"  id="per"  readonly="true" label="ค่าร้อยละ"/>
                <s:hidden  name="IndicatorsModel.in_score"  id="score"  readonly="true" label="คะแนนที่ได้"/>
                <s:hidden  name="IndicatorsModel.in_achieve"  id="ach"  readonly="true" label="การบรรลุเป้าหมาย"/>
                <s:hidden  name="IndicatorsModel.in_percent_add"  id="in_percent_add"  readonly="true" label="ร้อยละที่เพิ่มขึ้น"/>
            </s:form>

        </div>


    </div>
</div>



<script>
    function fnsave() {
        $("#form1").submit();
    }

    function AddAtt(no) {
        var newWindow = window.open('CalculateFormAtt?in_id=${in_id}&no=' + no, 'name', "height=200,width=300");
    }

    function DelAtt(ca_id) {
        var newWindow = window.open('CalculateDelete?ca_id=' + ca_id, 'name', "height=200,width=300");
        window.location.reload();
    }

    function pop(ip_position) {

        var newWindow = window.open('ReIndicatorsPersonnelForm?ip_position=' + ip_position + '&in_id=' +${in_id}, 'name', "height=200,width=300");
    }

    function del() {
        var newWindow = window.open('ReIndicatorsPersonnelForm?ip_position=3&in_id=' +${in_id}, 'name', "height=200,width=300");
    }

    function cal(id) {
        var newWindow = window.open('CalculateEdit?ca_id=' + id, 'name', "height=200,width=300");
    }

    function addcal(ca_id, ca_name, ca_comment, no) {
        var newWindow = window.open('CalculateForm?ca_id=' + ca_id + '&ca_name=' + ca_name + '&no=' + no + '&in_id=' +${in_id},
                'name', "height=400,width=600");
    }


    $('#sum1').text(Number($('#ba1').text()) + Number($('#mba1').text()) + Number($('#phd1').text()));
    $('#sum2').text(Number($('#ba2').text()) + Number($('#mba2').text()) + Number($('#phd2').text()));
    $('#gyear1').text($('#year1').text());
    $('#gsum1').text($('#sum1').text());
    $('#gphd1').text($('#phd1').text());
    $('#percent1').text(Number($('#phd1').text()) * 100 / $('#sum1').text());
     $("#per_old").val($("#percent1").text());
    $('#gyear2').text($('#year2').text());
    $('#gsum2').text($('#sum2').text());
    $('#gphd2').text($('#phd2').text());
    $('#percent2').text(Number($('#phd2').text()) * 100 / $('#sum2').text());
    $('#padd').text($('#percent2').text() - $('#percent1').text());
    $('#score1').text($('#percent2').text());
    $('#score2').text($('#percent2').text() * 5 /${in.in_target});

    $('#num_performance').text($('#score1').text());
    $('#in_score').text($('#score2').text());
    if ($('#in_score').text() > 5) {
        $('#in_score').text(5);
    }

    if ($('#num_performance').text() >= ${in.in_target}) {
        $('#achieve').text('ผ่าน');
        $('#ach').val(1);
    } else {
        $('#achieve').text('ไม่ผ่าน');
        $('#ach').val(0);
    }

    $(".panel-content").on('mousemove', function() {
        $('#in_num').val(Number($('#phd2').text()));
        $('#in_div').val(Number($('#sum2').text()));
        $('#per').val(Number($('#num_performance').text()));
        $('#score').val(Number($('#in_score').text()));
        $('#in_percent_add').val(Number($('#padd').text()));

    });


</script>