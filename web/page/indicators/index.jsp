<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="panel default">
    <div class="panel bg-lightBlue fg-white">
        <div class="grid fluid">
            <div class="row">
                <div class="span12 text-center" style="font-size: 20px">&nbsp  ตัวบ่งชี้</div>
            </div>
        </div>
    </div>
    <!--    <nav class="breadcrumbs">
            <ul>
                <li><a href="#">Admin</a></li>
                <li><a href="YearIndex">ประเมินคุณภาพ</a></li>
          
            </ul>
        </nav>-->
    <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
        <div class="text-right padding5"><a class="button success" href="IndicatorsForm?el_id=${el_id}">
                <span class="icon-plus"></span> เพิ่มตัวบ่งชี้</a>
        </div>
    </s:if>

    <div class="panel-content">

        <table class="table">
            <thead>
                <tr>
                    <td>ประเภท</td>
                    <td>ตัวบ่งชี้ที่</td>
                    <td>ชื่อตัวบ่งชี้</td>
                    <td>ชนิดตัวบ่งชี้</td>
                    <!--<td>ดูรายงาน</td>-->
                    <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
                        <td></td>
                    </s:if>

                </tr>
            </thead>
            <tbody>
                <s:iterator value="list" var="indicators">
                    <tr>
                        <td>${indicators.in_category}</td>
                        <td>${indicators.in_time}</td>

                        <td>

                            <c:choose>
                                <c:when test="${indicators.in_time =='2.2'}">
                                    <a href="TwoDotTwoIndex?in_id=${indicators.in_id}">${indicators.in_name}</a>
                                </c:when>
                                <c:when test="${indicators.in_time =='2.3'}">
                                    <a href="TwoDotThreeIndex?in_id=${indicators.in_id}">${indicators.in_name}</a>
                                </c:when>
                                <c:when test="${indicators.in_time =='4.3'}">
                                    <a href="FourDotThreeIndex?in_id=${indicators.in_id}">${indicators.in_name}</a>
                                </c:when>
                                <c:otherwise>
                                    <a href="IndicatorsDetail?in_id=${indicators.in_id}">${indicators.in_name}</a>
                                </c:otherwise>
                            </c:choose>


                        </td>

                        <td>${indicators.in_type}</td>
                        <!--<td><a href="ireport?in_id=${indicators.in_id}" target="_new">รายงาน</a></td>-->
                        <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
                            <td class="text-right">
                                <c:choose>
                                    <c:when test="${indicators.in_time =='2.2'}">
                                        <a href="TwoDotTwoIndex?in_id=${indicators.in_id}" class="button info"><i class="icon-search"></i> ดูรายละเอียด</a>
                                        <a href="TwoDotTwoEdit?in_id=${indicators.in_id}" class="button warning"><i class="icon-pencil"></i> แก้ไข</a>
                                        <a href="TwoDotTwoDelete?in_id=${indicators.in_id}" class="button danger"><i class="icon-cancel-2"></i> ลบ</a>
                                    </c:when>
                                    <c:when test="${indicators.in_time =='2.3'}">
                                        <a href="TwoDotThreeIndex?in_id=${indicators.in_id}" class="button info"><i class="icon-search"></i> ดูรายละเอียด</a>
                                        <a href="TwoDotThreeEdit?in_id=${indicators.in_id}" class="button warning"><i class="icon-pencil"></i> แก้ไข</a>
                                        <a href="TwoDotThreeDelete?in_id=${indicators.in_id}" class="button danger"><i class="icon-cancel-2"></i> ลบ</a>
                                    </c:when>
                                    <c:when test="${indicators.in_time =='4.3'}">
                                        <a href="FourDotThreeIndex?in_id=${indicators.in_id}" class="button info"><i class="icon-search"></i> ดูรายละเอียด</a>
                                        <a href="FourDotThreeEdit?in_id=${indicators.in_id}" class="button warning"><i class="icon-pencil"></i> แก้ไข</a>
                                        <a href="FourDotThreeDelete?in_id=${indicators.in_id}" class="button danger"><i class="icon-cancel-2"></i> ลบ</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="IndicatorsDetail?in_id=${indicators.in_id}" class="button info"><i class="icon-search"></i> ดูรายละเอียด</a>
                                        <a href="IndicatorsEdit?in_id=${indicators.in_id}" class="button warning"><i class="icon-pencil"></i> แก้ไข</a>
                                        <a href="IndicatorsDelete?in_id=${indicators.in_id}&el_id=${indicators.el_id}" class="button danger"><i class="icon-cancel-2"></i> ลบ</a>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </s:if>
                    </tr>
                </s:iterator>
            </tbody>
        </table>

    </div>
</div>


