<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="AttachmentsSave"  enctype="multipart/form-data" onsubmit="return closeSelf()" id="myform">
        <s:hidden name="at_id" label="รหัส"/>
        <s:file  name="at_name"  label="ไฟล์"/>
        <%--<s:textfield name="AttachmentsModel.at_name" label="ชื่อไฟล์แนบ"/>--%>
        <s:hidden name="AttachmentsModel.p_id" label="รหัส p_id"/>
        <s:hidden name="AttachmentsModel.pe_id" label="รหัส pe_id" value="%{pe_id}" id="pe_id"/>
        <s:submit value="บันทึก"  cssClass="success"/>
    </s:form>


</div>

<script type="text/javascript">
    function closeSelf() {
    window.opener.location.reload(true);
            alert("เพิ่มไฟล์สำเร็จ");
//            self.close();
    }
</script>