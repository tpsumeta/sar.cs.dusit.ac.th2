<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="CriteriaSave" method="post">
        <s:hidden name="cr_id" label="รหัส"/>
        <s:hidden  name="CriteriaModel.in_id"  label="in_id" type="number" value="%{in_id}"/>
        <s:textfield  name="CriteriaModel.performance"  label="ข้อ" type="number"/>
        <s:textfield  name="CriteriaModel.cr_score"  label="คะแนน" type="number"/>
        <s:textfield  name="CriteriaModel.cr_text"  label="คำอธิบาย"/>
        <s:submit value="บันทึก"/>
    </s:form>


</div>