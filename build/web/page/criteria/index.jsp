<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<div class="panel default">
    <div class="panel-header text-center">
        เกณฑ์การให้คะแนน
        <div class="text-right"><a class="button success" href="#" onclick="add();"><span class="icon-plus"></span> เพิ่มข้อ</a></div>
    </div>

    <div class="panel-content">

        <table class="table">
            <thead>
                <tr>
                    <td>ข้อ</td>
                    <td>คะแนน</td>
                    <td>คำอธิบาย</td>
                    <td>จัดการ</td>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="list" var="CrList">
                    <tr>
                        <td>${CrList.performance}</td>
                        <td>${CrList.cr_score}</td>
                        <td>${CrList.cr_text}</td>
                        <td>
                            <a href="#" onclick="edit(${CrList.cr_id});"><i class="icon-pencil"></i></a> 
                            <a href="#" onclick="del(${CrList.cr_id});">ลบ</a> 
                        </td>
                    </tr>
                </s:iterator>
            </tbody>
        </table>

    </div>
</div>



    <script>
    function add() {
    var newWindow = window.open('CriteriaForm?in_id=${in_id}', 'name', "height=400,width=600");
    }
    function edit(id) {
    var newWindow = window.open('CriteriaEdit?cr_id='+id+'&in_id=${CrList.in_id}', 'name', "height=400,width=600");
    }
    function del(id) {
    var newWindow = window.open('CriteriaDelete?cr_id='+id+'&in_id=${CrList.in_id}', 'name', "height=400,width=600");
    }
    </script>


