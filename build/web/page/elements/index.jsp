<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<div class="panel default">
    <div class="panel bg-lightBlue fg-white">
        <div class="grid fluid">
            <div class="row">
                <div class="span12 text-center" style="font-size: 20px">&nbsp องค์ประกอบ</div>
            </div>
        </div>
    </div>
    <!--    <nav class="breadcrumbs">
            <ul>
                <li><a href="#">Admin</a></li>
                <li><a href="YearIndex">ประเมินคุณภาพ</a></li>
       
            </ul>
        </nav>-->
    <div class="panel-content">
        <div class="text-right padding5">
            <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
                <!-- <a class="button success" href="ElementsFormGen"><span class="icon-target"></span> สร้างอัตโนมัติ</a> -->
                <a class="button success" href="ElementsForm?y_id=${y_id}"><span class="icon-plus-2"></span> เพิ่มองค์ประกอบ</a>
            </s:if>
            <a class="button success" href="SummaryReport?y_id=${y_id}" target="_blank"><span class="icon-printer"></span> รายงาน</a>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <td>องค์ประกอบที่</td>
                    <td>ชื่อองค์ประกอบ</td>
                    <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
                        <td class="text-right"></td>
                    </s:if>

                </tr>
            </thead>
            <tbody>
                <s:iterator value="elementsList" var="elements">
                    <tr>
                        <td>${elements.el_time}</td>
                        <td><a href="IndicatorsIndex?el_id=${elements.el_id}">${elements.el_name}</a></td>
                            <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
                            <td  class="text-right">
                                <a href="IndicatorsIndex?el_id=${elements.el_id}" class="button info">
                                    <i class="icon-search"></i> ดูรายละเอียด
                                </a>
                                <a href="ElementsEdit?y_id=${y_id}&el_id=${elements.el_id}" class="button warning">
                                    <i class="icon-pencil"></i> แก้ไข
                                </a>
                                <a href="ElementsDelete?y_id=${y_id}&el_id=${elements.el_id}" class="button danger">
                                    <i class="icon-remove"></i> ลบ
                                </a>
                            </td>
                        </s:if>
                    </tr>
                </s:iterator>
            </tbody>
        </table>

    </div>
</div>

