<%-- 
    Document   : index
    Created on : 16 มิ.ย. 2557, 22:31:22
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<div class="">
    <div class="panel default">
        <div class="panel bg-lightBlue fg-white">

            <div class="grid fluid">
                <div class="row">
                    <div class="span12 text-center" style="font-size: 20px">&nbsp ประเมินคุณภาพ</div>
                    <!--<div class="span3 text-right"><a href="YearForm" class="button primary"> + เพิ่มการประเมิน</a>&nbsp</div>-->
                </div>
            </div>

        </div>


        <div class="panel-content">
            <table class="table bordered">
                <tr><th>ปี</th><th>รายงานสรุป</th></tr>
                <s:iterator value="YearList" var="year">
                    <tr>
                        <td><a href="HomeIndex2?y_id=${year.y_id}">ประจำปีการศึกษา ${year.y_name}</a></td>
                        <td><a href="SummaryReport?y_id=${year.y_id}"><i class="icon-file-pdf"></i></a></td>
                    </tr>
                </s:iterator>
            </table>

        </div>
    </div>

</div>