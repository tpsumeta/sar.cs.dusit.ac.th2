<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>
<div class="container">
    <s:form action="ProsonnalSave" enctype="multipart/form-data" method="post" id="form1">
        <s:file name="p_img2"  label="รูปภาพ" />
        <s:hidden name="prosonnelModel.p_img"  />
        <s:radio label="การใช้งาน" name="prosonnelModel.p_active" list="# {'1':'ใช้งาน','0':'ปิดใช้งาน'}" />
        <s:textfield name="prosonnelModel.p_name" label="ชื่อ"/>
        <s:textfield name="prosonnelModel.p_tel" label="เบอร์โทร"/>
        <s:textfield name="prosonnelModel.p_email" label="อีเมล์l"/>
        <s:textfield name="prosonnelModel.p_idcard" label="รหัสบัตรประชาชน"/>
        <s:textfield name="prosonnelModel.p_user" label="p_user"/>
        <s:textfield name="prosonnelModel.p_pass" label="p_pass"/>
        <s:select label="สถานะ" 
                  list="# {'อาจารย์':'อาจารย์', 'ธุรการ':'ธุรการ', 'ผู้ดูแลระบบ':'ผู้ดูแลระบบ'}" 
                  name="prosonnelModel.p_status"  />
        <s:hidden name="p_id"/>
        <s:submit value="บันทึก" cssClass="info"/>
    </s:form>


</div>

<script type="text/javascript">
    
    $("#form1").submit(function(event) {
          var formData = new FormData(this);
        event.preventDefault();
        $.ajax({
            url: 'ProsonnalSave',
            data: formData,
            mimeType:"multipart/form-data",
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data) {
                alert("บันทึกข้อมูลแล้ว");
            }
        });
    });

</script>