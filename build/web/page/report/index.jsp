<%@page import="org.apache.pdfbox.encoding.DictionaryEncoding"%>
<%@page import="org.apache.pdfbox.cos.COSName"%>
<%@page import="org.apache.pdfbox.cos.COSDictionary"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.apache.pdfbox.encoding.WinAnsiEncoding"%>
<%@page import="org.apache.pdfbox.pdmodel.font.PDTrueTypeFont"%>
<%@page import="org.apache.pdfbox.pdmodel.ConformingPDDocument"%>
<%@page import="java.io.File"%>
<%@ page import="org.apache.pdfbox.pdmodel.PDDocument" %>
<%@ page import="org.apache.pdfbox.pdmodel.PDPage" %>
<%@ page import="org.apache.pdfbox.pdmodel.edit.PDPageContentStream" %>
<%@ page import="org.apache.pdfbox.pdmodel.font.PDFont" %>
<%@ page import="org.apache.pdfbox.pdmodel.font.PDType1Font" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Report</title>
    </head>
    <body>


        <%
            String year = "2556";

            try {
                PDDocument document = new PDDocument();
                PDPage pdf = new PDPage();
                document.addPage(pdf);

                // Create a new font object selecting one of the PDF base fonts
//                PDFont font = PDType1Font.HELVETICA;
//                 PDTrueTypeFont font = PDTrueTypeFont.loadTTF(pdfFile, new File("fonts/VREMACCI.TTF"));
//                font.setEncoding(new WinAnsiEncoding());
//                PDTrueTypeFont font = PDTrueTypeFont.loadTTF(document, new File("fonts/THSarabunNew.ttf"));
                PDTrueTypeFont font = PDTrueTypeFont.loadTTF(document, new File(application.getRealPath("/fonts/tahoma.ttf")));
//                font.setWidths(PDType1Font.HELVETICA.getWidths());

                // Start a new content stream which will "hold" the to be created content
                PDPageContentStream contentStream = new PDPageContentStream(document, pdf);

                String arabic_text = new String("yyyสวัสดีxxx".getBytes(), "UTF-8");
//                  String language = new  String(request.getParameter("language").getBytes("ISO8859_1"),"UTF-8");

                // Define a text content stream using the selected font, moving the cursor and drawing the text "Hello World"
                contentStream.beginText();
                contentStream.setFont(font, 20);
                contentStream.moveTextPositionByAmount(190, 720);
                contentStream.drawString("SAR REPORT YEAR ก" + year);

                contentStream.endText();

                contentStream.beginText();
                contentStream.setFont(font, 18);
                contentStream.moveTextPositionByAmount(100, 700);
                contentStream.drawString("Welcome come to my Report " + arabic_text);

                contentStream.endText();

                // Make sure that the content stream is closed:
                contentStream.close();

                // Save the results and ensure that the document is properly closed:
                document.save(application.getRealPath("/report/รายงานการประเมินคุณภาพ" + year + ".pdf"));
                document.close();

                out.println("PDF Created to." + application.getRealPath("/report/"));

            } catch (Exception e) {
                e.printStackTrace();
            }


        %>
    </body>
</html>