<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<div class="panel default">
      <div class="panel-header text-center bg-lightBlue fg-white">
        องค์ประกอบ
        <div class="text-right">
            <a class="button success" href="PerformanceForm?in_id=${in_id}"><span class="icon-plus"></span>ผลการดำเนินงาน</a>
        </div>
    </div>

    <div class="panel-content">

        <table class="table">
            <thead>
                <tr>
                    <td>ข้อ</td>
                    <td>เกณฑ์มาตรฐาน</td>
                    <td>จัดการ</td>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="list" var="performance">
                    <tr>
                        <td>${elements.pe_point}</td>
                        <td><a href="IndicatorsIndex?pe_id=${performance.pe_id}">${performance.p_standard}</a></td>
                        <td>
                            <a href="PerformanceEdit?pe_id=${performance.pe_id}"><i class="icon-pencil"></i></a>
                            <a href="PerformanceDelete?pe_id=${performance.pe_id}"><i class="icon-cancel-2"></i></a>
                        </td>
                    </tr>
                </s:iterator>
            </tbody>
        </table>
            
    </div>
</div>


