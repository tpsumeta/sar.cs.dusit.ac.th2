<%-- 
    Document   : add
    Created on : 14 มิ.ย. 2557, 0:03:33
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags"  prefix="s"%>

<s:form action="IndicatorsSave" method="post">
    <s:hidden name="in_id" label="รหัส"/>        
    <s:textfield  name="IndicatorsModel.in_category"  label="ประเภทตัวบ่งชี้" placeholder="สกอ สมส"/>
    <s:textfield  name="IndicatorsModel.in_time"  label="ตัวบ่งชี้ที่" placeholder="ex 1.1"/>
    <s:textfield name="IndicatorsModel.in_name" label="ชื่อ" placeholder="การบวนการแผนพัฒนา" size="100"/>
    <s:select list="listType" name="IndicatorsModel.in_type" label="ชนิดตัวบ่งชี้"/>
    <s:hidden  name="IndicatorsModel.in_criteria"  label="เกณฑ์การประเมิน" />
    <s:textarea name="IndicatorsModel.in_good" label="จุดแข็ง" placeholder="จุดแข็ง" id="i"  />
    <s:textarea name="IndicatorsModel.in_development" label="จุดที่ควรพัฒนา" placeholder="จุดที่ควรพัฒนา"  />
    <s:textarea name="IndicatorsModel.in_practices" label="วิธีปฏิบัติที่ดี" placeholder="วิธีปฏิบัติที่ดี" rows="5" />
    <s:textfield name="IndicatorsModel.in_target"  label="ค่าเป้าหมาย" type="number"/>
    <s:hidden name="IndicatorsModel.in_num_performance"  label="ค่าเป้าหมาย" type="number"/>
    <s:hidden name="IndicatorsModel.in_score"  label="ค่าเป้าหมาย" type="number"/>
    <s:hidden name="IndicatorsModel.in_achieve"  label="ค่าเป้าหมาย" type="number"/>
    <s:hidden name="IndicatorsModel.el_id" type="number" label="รหัสองค์ประกอบ"/>
    <s:submit value="บันทึก" cssClass="success"/>

</s:form>



<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code  insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        content_css: "css/content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
</script>