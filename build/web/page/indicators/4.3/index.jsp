<%-- 
    Document   : prosonnel_data
    Created on : 13 มิ.ย. 2557, 19:22:14
    Autdor     : Top_Sumeta
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@include file="../../../conn.jsp" %>

<sql:query dataSource="${snapshot}" var="att">
    SELECT * from re_indicators_personnel
    left join personnel
    on re_indicators_personnel.p_id = personnel.p_id
    where in_id=${in_id}  and personnel.p_id=${loginID} and ip_position =0
</sql:query>
<c:if test="${att.rowCount < 1 }">
    <c:if test="${status != 'ผู้ดูแลระบบ' }">
        <script>
            alert("${loginUser} คุณไม่มีสิทธิถึงใช้หน้านี้");
            history.back()
        </script>
    </c:if>
</c:if> 

<div class="panel default">
    <div class="panel-header text-center bg-lightBlue fg-white">
        ตัวบ่งชี้   
    </div>

    <div class="panel-content">
        <a href="FourDotThreeReport?in_id=${in_id}" target="_new"><button class="info"><i class="icon-printer"></i> รายงาน</button></a>   
        <a href="IndicatorsEdit?in_id=${in_id}" target="_new"><button class="warning"><i class="icon-pencil"></i> แก้ไข</button></a>   
        <br/><br/>
        <table class="table bordered">
            <s:iterator value="list" var="indicators">
                <tr><td width="10%">ตัวบ่งชี้</td><td>${indicators.in_category}</td></tr>
                <tr><td>ที่</td><td>${indicators.in_time}</td></tr>
                <tr><td>ชื่อ</td><td>${indicators.in_name}</td></tr>
            </s:iterator>
        </table>

        <a onclick="AddPerformance();"> + เพิ่ม </a>
        <table class="table bordered">
            <tr class="panel-header">
                <th>ลำดับ</th>
                <th>ข้อมูล</th>
                <th>จำนวนอาจารย์และนักวิจัย</th>
                <th>เงินสนับสนุนภายในภายนอก</th>
                <th>เงินเฉลียต่อคน</th>
                <th>คะแนนที่ได้</th>
                <th>หลักฐาน</th>
                <th>จัดการ</th>
            </tr>
            <s:set value="0" var="sum" />
            <s:set value="0" var="sum_have" />
            <s:iterator value="performanceList" var="performance">
                <tr>
                    <td>${performance.pe_point}</td>
                    <td>${performance.pe_data}</td>
                    <td>${performance.pe_teacher}</td>
                    <td>${performance.pe_monny}</td>
                    <td name="pe_monny_avg">${performance.pe_monny_avg}</td>
                    <td name="pe_score" id="s1">${performance.pe_score}</td>

                    <td>
                        <a onclick="AddAtt(${performance.pe_id})"><i class="icon-plus-2"></i></a>
                            <sql:query dataSource="${snapshot}" var="re">
                            SELECT * FROM  `attachments` where pe_id=${performance.pe_id}
                        </sql:query>
                        <c:forEach var="row" items="${re.rows}">
                            ${row.at_name} <a  onclick="DelAtt(${row.at_id});" target="_new">-</a><br/>
                        </c:forEach>
                    </td>
                    <td>
                        <a href="PerformanceEdit2?in_id=${performance.in_id}&pe_id=${performance.pe_id}">แก้ไข</a> / 
                        <a onclick="DelPerformance(${performance.in_id},${performance.pe_id});">ลบ</a>
                    </td>
                </tr>
            </s:iterator>
            <tr>
                <td></td>
                <td></td>
                <th colspan="3">คะแนนเฉลี่ย</th>
                <td id="score1"></td>
                <td></td>
                <td></td>
            </tr>

        </table>
        หมายเหตุ <br/>
        หลักฐานอ้างอิง <br/>


        <table class="table bordered">
            <tr><th colspan="4" class="panel-header">การประเมินตนเอง   
                    <!--<a id="createWindowDraggable">[เพิ่ม]</a>-->
                </th>
            </tr>
            <tr><th>เป้าหมาย</th><th>ผลการดำเนินงาน</th><th>คะแนนการประเมินตนเอง</th><th>การบรรลุเป้าหมาย</th></tr>
            <tr>
                <td>${indicators.in_target} บาท/คน</td>
                <td id="sum_avg">${sum} บาท/คน </td>
                <td id="score2">${indicators.in_score}  คะแนน  </td>
                <td id="achieve1"></td>
            </tr>
        </table>

        <b> จุดแข็ง/แนวทางเสริมจุดแข็ง</b> <br/>
        ${indicators.in_good}<br/><br/>
        <b>จุดที่ควรพัฒนา/ข้อเสนอแนะในการปรับปรุง</b><br/>
        ${indicators.in_development}<br/><br/>
        <b>จุดที่ควรพัฒนา/ข้อเสนอแนะในการปรับปรุง</b><br/>
        ${indicators.in_practices}
        <br/><br/>

        <sql:query dataSource="${snapshot}" var="re">
            SELECT * from re_indicators_personnel
            left join personnel
            on re_indicators_personnel.p_id = personnel.p_id
            where in_id=${in_id} 
        </sql:query>

        <h4>ผู้กำกับ <a  onclick="AddPosition(1)">+</a></h4>
        <c:forEach var="row" items="${re.rows}">
            <c:if test="${row.ip_position == 1}">
                ${row.p_name} <a onclick="DelPosition(${row.ip_id})">-</a><br/>
            </c:if>
        </c:forEach>

        <h4>ผู้จัดเก็บข้อมูล  <a  onclick="AddPosition(2)">+</a></h4> 
        <c:forEach var="row" items="${re.rows}">
            <c:if test="${row.ip_position == 2}">
                ${row.p_name} <a onclick="DelPosition(${row.ip_id})">-</a><br/>
            </c:if>
        </c:forEach>

        <h4>ผู้เขียนผลการดำเนินการ  <a  onclick="AddPosition(3)">+</a></h4> 
        <c:forEach var="row" items="${re.rows}">
            <c:if test="${row.ip_position == 3}">
                ${row.p_name} <a onclick="DelPosition(${row.ip_id})">-</a><br/>
            </c:if>
        </c:forEach>

      <c:if test="${status == 'ผู้ดูแลระบบ' }">
            <hr/>
            <h4>ผู้มีสิทธิเข้าใช้  <a class="mini info"  onclick="pop(0)"><i class="icon-plus-2"></i></a></h4> 
                    <c:forEach var="row" items="${re.rows}">
                        <c:if test="${row.ip_position == 0}">
                            ${row.p_name} <a name="del1" value="${row.ip_id}"><i class="icon-remove fg-red" ></i></a><br/>
                    </c:if>
                </c:forEach>
            </c:if>

        <center>
            <s:form action="IndicatorsSave" method="post"  id="form1">
                <s:hidden name="in_id" label="รหัส"/>     
                <s:hidden name="IndicatorsModel.in_num_performance" id="per" label="ผลการดำเนินงาน"/>
                <s:hidden name="IndicatorsModel.in_score" id="in_score"  label="คะแนน"/>
                <s:hidden name="IndicatorsModel.in_achieve"  id="in_achieve" label="บรรลุ"/>
                <s:hidden  name="IndicatorsModel.in_category"  label="หมวดหมู่" value="%{#indicators.in_category}"/>  
                <s:hidden  name="IndicatorsModel.in_time"  label="ตัวบ่งชี้ที่" value="%{#indicators.in_time}"/>
                <s:hidden  name="IndicatorsModel.in_name"  label="ชื่อ" value="%{#indicators.in_name}"/>
                <s:hidden  name="IndicatorsModel.in_type"  label="ชนิด" value="%{#indicators.in_type}"/>
                <s:hidden  name="IndicatorsModel.in_good"  label="จุดแข็ง" value="%{#indicators.in_good}"/>
                <s:hidden  name="IndicatorsModel.in_development"  label="จุดแข็ง" value="%{#indicators.in_development}"/>
                <s:hidden  name="IndicatorsModel.in_practices"  label="จุดแข็ง" value="%{#indicators.in_practices}"/>
                <s:hidden  name="IndicatorsModel.el_id" value="%{#indicators.el_id}"/>
                <s:hidden  name="IndicatorsModel.in_target"   value="%{#indicators.in_target}"  id="target"  label="เป้าหมาย"/>
                <s:hidden  id="sum_have"  value="%{#sum_have}" label="มีหรือไม่" />
                <%--<s:submit value="บันทึก" cssClass="btn success"/>--%>
            </s:form>
        </center>

    </div>
</div>



<script>

    var sum_score = 0;
    var sum_avg = 0;
    $("td[name=pe_score]").each(function() {
        sum_score += parseFloat(($(this).text()));
    });
    $("td[name=pe_monny_avg]").each(function() {
        sum_avg += parseFloat(($(this).text()));
    });

//    คะแนน
    if (sum_score > 5) {
        sum_score = 5;
    }
    $("#score1 ,#score2").text(sum_score.toFixed(2) + ' คะแนน');

//    เงินเฉลี่ย
    $("#sum_avg").text(sum_avg.toFixed(2) + ' บาท/คน');

//   ผลการดำเนินการ
    $("#per").val(sum_avg);
// คะแนน
    $("#in_score").val(sum_score);

//การบรรลุเป้าหมาย
    if (parseFloat($("#per").val()) >= parseFloat($("#target").val())) {
        $("#achieve1").text("บรรลุเป้าหมาย");
        $("#in_achieve").val(1);
    } else {
        $("#achieve1").text("ไม่บรรลุเป้าหมาย");
        $("#in_achieve").val(0);
    }

//    $(window).bind("beforeunload", function() { 
//    return confirm("Do you really want to close?"); 
//});

    $("a").on('mouseover', function() {
        $("#form1").submit();
    });

    $("#form1").submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "IndicatorsSave",
            data: $(this).serialize(),
            success: function() {
                console.log("SAVA OK!!");
            }
        });
    });

    function AddAtt(pe_id) {
        var newWindow = window.open('AttachmentsForm?pe_id=' + pe_id, 'name', "height=200,width=300");
    }

    function DelAtt(at_id) {
        $.get("AttachmentsDelete?at_id=" + at_id, function() {
            alert("ลบไฟล์สำเร็จแล้ว");
            window.location.reload();
        });
    }


    function AddPerformance() {
        var newWindow = window.open('PerformanceForm2?in_id=' +${in_id}, 'name', "height=400,width=600");
    }
    function DelPerformance(in_id, pe_id) {
        $.get('PerformanceDelete?in_id=' + in_id + '&pe_id=' + pe_id, function() {
            alert("ลบสำเร็จ");
            window.location.reload();
        });
    }

    function AddPosition(ip_position) {
        var newWindow = window.open('ReIndicatorsPersonnelForm?ip_position=' + ip_position + '&in_id=' +${in_id}, 'nam e', "height=400,width=600");
    }
    function DelPosition(ip_id) {
        $.get("ReIndicatorsPersonnelDelete?ip_id=" + ip_id, function() {
            alert("DELETE OK");
            window.location.reload();
        });
    }

</script>
