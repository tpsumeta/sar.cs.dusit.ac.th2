<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:form action="TwoDotTwoSave" method="post">
    <s:textfield name="in_id" label="รหัส"/>
    <s:textfield  name="IndicatorsModel.in_category"  label="หมวดหมู่"/>
    <s:textfield  name="IndicatorsModel.in_time"  label="ตัวบ่งชี้ที่"/>
    <s:textfield  name="IndicatorsModel.in_name"  label="ชื่อ"/>
    <s:textfield  name="IndicatorsModel.in_type"  label="ชนิด"/>
    <s:textfield  name="IndicatorsModel.in_target"  label="เป้าหมาย" id="target"/>
    <s:textarea  name="IndicatorsModel.in_criteria"  label="เกณฑ์การประเมิน"/>
    <s:textarea  name="IndicatorsModel.in_good"  label="จุดแข็ง"/>
    <s:textarea  name="IndicatorsModel.in_development"  label="จุดควรพัฒนา"/>
    <s:textarea  name="IndicatorsModel.in_practices"  label="แนวทางปฏัติที่ดี"/>
    <s:hidden  name="IndicatorsModel.el_id"/>
    <s:hidden  name="IndicatorsModel.in_numerator"  label="อาจารย์ที่วุฒิปริญาเอก" type="number" id="in_num"  onchange="clickAdd();"/>
    <s:hidden  name="IndicatorsModel.in_divisor"  label="อาจารย์ทั้งหมด" type="number" id="in_div"  onchange="clickAdd();"/>
    <s:hidden  name="IndicatorsModel.in_num_performance"  id="per"  readonly="true" label="ค่าร้อยละ"/>
    <s:hidden  name="IndicatorsModel.in_score"  id="score"  readonly="true" label="คะแนนที่ได้"/>
    <s:hidden  name="IndicatorsModel.in_achieve"  id="achieve"  readonly="true" label="การบรรลุเป้าหมาย"/>
    <s:hidden  name="IndicatorsModel.percent_old"  id="per_old"  readonly="true" label="ค่าร้อยละของปีที่แล้ว"/>
    <s:submit value="บันทึก"/>
</s:form>

<script>


    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code  insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        content_css: "css/content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | ink image | print preview media fullpage | forecolor backcolor emoticons",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });

</script>


