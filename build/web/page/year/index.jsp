<%-- 
    Document   : index
    Created on : 11 มิ.ย. 2557, 1:34:16
    Author     : dev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<div class="">
    <div class="panel default">
        <div class="panel bg-lightBlue fg-white">

            <div class="grid fluid">
                <div class="row">
                    <div class="span12 text-center" style="font-size: 20px">&nbsp ประเมินคุณภาพ</div>
                    <!--<div class="span3 text-right"><a href="YearForm" class="button primary"> + เพิ่มการประเมิน</a>&nbsp</div>-->
                </div>
            </div>

        </div>
    <!--        <nav class="breadcrumbs">
                <ul>
                    <li><a href="#">Admin</a></li>
                    <li><a href="YearIndex">ประเมินคุณภาพ</a></li>
                </ul>
            </nav>-->

        <div class="panel-content">
            <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" > 
                <div class="padding5"><a href="YearForm" class="button primary"><i class="icon icon-plus-2"></i> เพิ่มการประเมิน</a></div>
            </s:if>
            <table class="table bordered">
                <tr><th>ปี</th>
                        <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" > 
                        <th>จัดการ</th>
                        </s:if>
                </tr>
                <s:iterator value="YearList" var="year">
                    <tr>
                        <td><a href="ElementsIndex?y_id=${year.y_id}">${year.y_name}</a></td>
                        <!--<td><a href="YearReport?y_id=${year.y_id}">ดูรายงาน</a></td>-->

                        <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >      
                            <!--ตรวจสอบสถานะของผู้ใช้-->
                            <td>        
                                <a class="button success" href="ElementsIndex?y_id=${year.y_id}">ดูรายละเอียด</a>
                                <a href="#" class="button warning" onclick="Edit(${year.y_id});">แก้ไข </a>
                                <!--<a href="YearDelete?y_id=${year.y_id}" class="button danger">ลบ</a>-->
                            </td>
                        </s:if>
                    </tr>
                </s:iterator>
            </table>

        </div>
    </div>

</div>

<script>
    function Edit(y_id) {
        var newWindow = window.open('YearEdit?y_id=' + y_id, 'name', " top=100,left=100,height=400,width=600");
    }
</script>

