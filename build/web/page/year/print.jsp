<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page  import="java.util.HashMap"%>
<%@ page  import="java.util.Map"%>
<%@ page  import="net.sf.jasperreports.engine.*"%>



<%@include file="../../conn.jsp" %>


<div class="container">
    <div class="panel default">



        <s:iterator value="YearList" var="year">
            <div style="text-align: center">

                <h3>รายงานการประเมินคุณภาพประจำปี  ${year.y_name}</h3>
            </div>
        </s:iterator>

        <sql:query dataSource="${snapshot}" var="result" >
            SELECT * from sar_elements where y_id =${year.y_id};
        </sql:query>



        <c:forEach var="row" items="${result.rows}">
            <h4>องค์ประกอบที่ ${row.el_time} ${row.el_name}</h4>
            <div style="padding-right: 2%">
                <sql:query dataSource="${snapshot}" var="re">
                    SELECT * from sar_indicators where el_id =${row.el_id};
                </sql:query>
                <c:forEach var="ro" items="${re.rows}">
                    <c:set value="0" var="sum"/>
                    <ul><b>ตัวบ่งชี้ที่ ${ro.in_time} ${ro.in_name}</b></ul>
                    <ul>ชนิดตัวบ่งชี้ ${ro.in_type}</ul>

                    <sql:query dataSource="${snapshot}" var="re_per">
                        SELECT * from sar_performance  where in_id =${ro.in_id};
                    </sql:query>

                    <ul>
                        เกณฑ์มาตรฐาน
                        <table border="1">
                            <tr><th>ข้อ</th><th>มี</th><th style="width: 75%">เกณฑ์การประเมิน</th><th>ผลการดำเนินงาน</th><th>หลักฐาน</th></tr>
                                    <c:forEach var="ro_per" items="${re_per.rows}">
                                <tr>
                                    <td>${ro_per.pe_point}</td>
                                    <td>

                                        <c:choose>
                                            <c:when test="${ro_per.pe_have==1}">มี
                                                <c:set value="${sum+1}" var="sum"/>
                                            </c:when>
                                            <c:otherwise>ไม่มี</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>${ro_per.p_standard}</td>
                                    <td>${ro_per.p_performance}</td>
                                    <td>
                                        <sql:query dataSource="${snapshot}" var="att">
                                            SELECT * from attachments where pe_id=${ro_per.pe_id}
                                        </sql:query>
                                        <c:forEach var="a" items="${att.rows}">
                                          <a href="image/${a.at_link}">${a.at_name}</a>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>    
                    </ul>
                    <ul>
                        การประเมินตนเอง
                        <table border="1">
                            <tr><th>เป้าหมาย</th><th>ผลดำเนินงาน</th><th>คะแนนการประเมินตนเอง</th><th>บรรลุเป้าหมาย</th></tr>
                            <tr><td>4 ข้อ</td><td>${sum} ข้อ</td><td>5 คะแนน</td><td>บรรลุ</td></tr>

                        </table>

                    </ul>
                    <ul>จุดแข็ง/แนวทางการเสริมจุดแข็ง</ul>
                    <ul>จุดที่ควรพัฒนา</ul>
                    <ul>วิธีปฏิบัติที่ดี</ul>
                    <ul>ผู้กำกับ</ul>
                    <ul>ผู้จัดเก็บข้อมูล</ul>
                    <ul>ผู้เขียนผลการดำเนินงาน</ul>
                    <br/>
                </c:forEach>
            </div>
            <br/>
            <hr/>
        </c:forEach>

    </div>

</div>



