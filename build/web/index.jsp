
<%-- 
    Document   : index
    Created on : 9 มิ.ย. 2557, 15:47:55
    Author     : Top_Sumeta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s"  uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SDU SAR</title>
        <!--css-->
        <link rel="stylesheet" href="css/metro-bootstrap.min.css">
        <link rel="stylesheet" href="css/metro-bootstrap-responsive.min.css">
        <link rel="stylesheet" href="css/iconFont.min.css">
        <link href="css/docs.css" rel="stylesheet">

        <!--js-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.widget.min.js"></script>
        <script src="js/metro.min.js"></script>
        <script src="js/docs.js"></script>
        <script src="js/load-metro.js"></script>
        <script src="js/tinymce/tinymce.min.js"></script>


    </head>
    <body class="metro">
        <div class="navigation-bar default">
            <div class="navigation-bar-content container">
                <a href="HomeIndex" class="element"><span class="icon-grid-view"></span> SAR CS SDU<sup>1.0</sup></a>
                <span class="element-divider"></span>
                <a class="element1 pull-menu" href="#"></a>
                <ul class="element-menu">
                    <li><a  href="HomeIndex"><i class="icon-home">&nbsp</i>หน้าแรก</a></li>
                    <!--                    <li>
                                            <a class="dropdown-toggle" href="#"><i class="icon-clipboard-2">&nbsp</i> รายงานการประเมินตนเอง&nbsp</a>
                                            <ul class="dropdown-menu default" data-role="dropdown">
                                                <li><a href="#">ข้อมูลพื้นฐาน</a></li>
                                                <li><a href="#">สภาพการดำเนินงาน</a></li>
                                                <li><a href="#">ผลการประเมินตนเอง</a></li>
                                                <li><a href="#">สรุปผลการประเมิน</a></li>
                                                <li><a href="#">Common Data Set</a></li>
                                                <li><a href="#">เอกสารที่เกี่ยวข้อง</a></li>
                                            </ul>
                                        </li>-->
                    <!--<li><a  href="sarindex">เปรียบเทียบข้อมูล</a></li>-->
                    <!--<li><a href="ContactIndex"><i class="icon-mail">&nbsp</i>ติดต่อสอบถาม</a></li>-->

                    <%--<s:if test="%{#session.loginUser == 'admin'}" >--%>
                    <s:if test="%{#session.status == 'ผู้ดูแลระบบ'}" >
                        <li>
                            <a href="" class="dropdown-toggle"><i class="icon-cog">&nbsp</i> ดูแลระบบ</a>
                            <ul class="dropdown-menu default" data-role="dropdown">
                                <li><a href="YearIndex"><i class="icon-flag">&nbsp</i>ประเมินคุณภาพ</a></li>
                                <li><a href="ProsonnalIndex"><i class="icon-user-2">&nbsp</i>บุคลากร</a></li>
                            </ul>
                        </li>
                    </s:if>

                    <s:if test="%{#session.login == 'true'}" >
                        <s:if test="%{#session.status == 'อาจารย์'}" >
                            <li><a href="YearIndex"><i class="icon-flag">&nbsp</i>ประเมินคุณภาพ</a></li>
                            </s:if>
                        <a title="ออกจากระบบ" href="Logout" class="element place-right"><span class="icon-locked"></span>ออกจากระบบ</a>
                        <span class="element-divider place-right"></span>
                        <div class="element place-right" title="ผู้ใช้">
                            <span class="icon-user-3"></span> <span class="github-watchers"></span>
                            ${loginUser}
                        </div>


                    </s:if>      
                    <s:else>
                        <div class="text-right">
                            <li>
                                <form action="Login" method="post" style="padding-top: 10px">  
                                    <input placeholder="User" type="text" size="9" name="user">
                                    <input placeholder="Password" type="password" size="9" name="pass">
                                    <button class="btn primary">เข้าสู่ระบบ</button>
                                </form>
                            </li>
                        </div>
                    </s:else>

            </div>


            <!--<div class="no-tablet-portrait no-phone">-->
            <!--<span class="element-divider place-right"></span>-->
            <!--<a title="รีเฟสหน้าจอ" href="" onclick="windows.location.reload();" class="element place-right"><span class="icon-spin"></span></a>-->
            <!--<span class="element-divider place-right"></span>-->
            <!--<a title="Print" href="" onclick="print();" class="element place-right"><span class="icon-printer"></span></a>-->
            <!--<span class="element-divider place-right"></span>-->
            <!--</div>-->

        </div>
    </div>


    <div style="padding: 5px">
        <s:if test="url != null">
            <jsp:include page="${url}" />
        </s:if>
    </div>


</body>
</html>
