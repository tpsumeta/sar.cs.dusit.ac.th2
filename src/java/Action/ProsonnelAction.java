package Action;

import java.util.List;
import Model.ProsonnelModel;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

//import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

public class ProsonnelAction extends IndexAction implements ServletRequestAware {

    private File p_img2;
    private String p_img2FileName;
    private List ProsonnelList;
    private ProsonnelModel prosonnelModel;
    private int p_id;
    private static List listImage;
    private HttpServletRequest servletRequest;
    private int p_active;

    public ProsonnelAction() {
        prosonnelModel = new ProsonnelModel();

//        if (listImage == null) {
//            listImage = new ArrayList();
//
//        }
    }

    public List getProsonnelList() {
        return ProsonnelList;
    }

    public void setProsonnelList(List ProsonnelList) {
        this.ProsonnelList = ProsonnelList;
    }

    public ProsonnelModel getProsonnelModel() {
        return prosonnelModel;
    }

    public void setProsonnelModel(ProsonnelModel prosonnelModel) {
        this.prosonnelModel = prosonnelModel;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public static List getListImage() {
        return listImage;
    }

    public static void setListImage(List listImage) {
        ProsonnelAction.listImage = listImage;
    }

    public File getP_img2() {
        return p_img2;
    }

    public void setP_img2(File p_img2) {
        this.p_img2 = p_img2;
    }

    public String getP_img2FileName() {
        return p_img2FileName;
    }

    public void setP_img2FileName(String p_img2FileName) {
        this.p_img2FileName = p_img2FileName;
    }

    public int getP_active() {
        return p_active;
    }

    public void setP_active(int p_active) {
        this.p_active = p_active;
    }

    public String index() {

        if (p_active == 0) {
            prosonnelModel.setP_active(p_active);
            ProsonnelList = prosonnelModel.active();
        } else if (p_active == 1) {
            prosonnelModel.setP_active(p_active);
            ProsonnelList = prosonnelModel.active();
        } else if (p_active == 9) {
            ProsonnelList = prosonnelModel.list();
        }
        return SUCCESS;
    }

    public String save() throws IOException {

        if (getP_img2() != null) {
            String filePath = servletRequest.getRealPath("/image/");
            File destinationFie = new File(filePath, p_img2FileName);
            FileUtils.copyFile(p_img2, destinationFie);
        }
        if (p_id == 0) {
            prosonnelModel.setP_img(p_img2FileName);
            prosonnelModel.save(prosonnelModel);
        } else {
            if (getP_img2() != null) {
                prosonnelModel.setP_img(p_img2FileName);
            }
            prosonnelModel.update(prosonnelModel, p_id);
            setP_id(0);
        }
        ProsonnelList = prosonnelModel.list();
        return SUCCESS;
    }

    public String edit() {
        prosonnelModel = prosonnelModel.find(p_id);
        ProsonnelList = prosonnelModel.list();
        return SUCCESS;
    }

    public String delete() {
        prosonnelModel.delete(p_id);
        ProsonnelList = prosonnelModel.list();
        return SUCCESS;
    }

    public String where() {
        prosonnelModel.setP_id(p_id);
        ProsonnelList = prosonnelModel.where();
        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

}
