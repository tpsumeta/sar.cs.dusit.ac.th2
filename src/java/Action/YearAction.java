package Action;

import java.util.List;
import Model.YearModel;

/**
 *
 * @author Top_Sumeta <tpsumeta@gmail.com>
 */
public class YearAction extends IndexAction {

    private List YearList;
    private YearModel yearModel;
    private int y_id;
   

 

    public YearAction() {
        yearModel = new YearModel();
    }

    public List getYearList() {
        return YearList;
    }

    public void setYearList(List YearList) {
        this.YearList = YearList;
    }

    public YearModel getYearModel() {
        return yearModel;
    }

    public void setYearModel(YearModel yearModel) {
        this.yearModel = yearModel;
    }

    public int getY_id() {
        return y_id;
    }

    public void setY_id(int y_id) {
        this.y_id = y_id;
    }

    public String index() {
        YearList = yearModel.list();
        return SUCCESS;
    }

    public String save() {
        if (y_id == 0) {
            yearModel.save(yearModel);
        } else {
            yearModel.update(yearModel, y_id);
            setY_id(0);
        }
        YearList = yearModel.list();
        return SUCCESS;
    }

    public String edit() {
        yearModel = yearModel.find(y_id);
        return SUCCESS;
    }

    public String delete() {

        return SUCCESS;
    }

    public String report() {
        yearModel.setY_id(y_id);
        YearList = yearModel.where();
        return SUCCESS;
    }

    public String json() {

        return SUCCESS;
    }
}
