package Action;

import Model.ProsonnelModel;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

/**
 * เข้าสู่ระบบ
 *
 * @author Top_Sumeta ; emial : tpsumeta@gmail.com
 */
public class LoginAction extends IndexAction implements SessionAware {

    public String user;
    public String pass;
    public String Finish;
    SessionMap<String, String> sessionmap;
    private ProsonnelModel prosonnelModel;
    private List ProLlist;

    public LoginAction() {
        prosonnelModel = new ProsonnelModel();
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getFinish() {
        return Finish;
    }

    public void setFinish(String Finish) {
        this.Finish = Finish;
    }

    public ProsonnelModel getProsonnelModel() {
        return prosonnelModel;
    }

    public void setProsonnelModel(ProsonnelModel prosonnelModel) {
        this.prosonnelModel = prosonnelModel;
    }

    public List getProLlist() {
        return ProLlist;
    }

    public void setProLlist(List ProLlist) {
        this.ProLlist = ProLlist;
    }

    public String doLogin() {
        prosonnelModel.setP_user(user);
        prosonnelModel.setP_pass(pass);
        ProLlist = prosonnelModel.login();
        String status = null;
        Integer p_id = null;
        for (Iterator iterator = ProLlist.iterator(); iterator.hasNext();) {
            prosonnelModel = (ProsonnelModel) iterator.next();
            status = prosonnelModel.getP_status();
            p_id = prosonnelModel.getP_id();
        }

        if (!ProLlist.isEmpty()) {
            sessionmap.put("loginUser", user);   // เพิ่ม session name =  loginUser  , value = user
            sessionmap.put("login", "true"); // เพิ่ม session name = login , value = true
            sessionmap.put("loginID", p_id.toString()); // เพิ่ม session name = login , value = true
            sessionmap.put("status", status); // เพิ่ม status loginUser value = status
            setFinish(status);
        } else {
            setFinish("LOGIN ERROR!!!");   // ถ้าไม่สามารถเข้าสู่ระบบได้
        }
        return SUCCESS;
    }

    //   set Sestion
    @Override
    public void setSession(Map map) {
        sessionmap = (SessionMap) map;

    }

    // ออกจากระบบ
    public String logout() {
        sessionmap.invalidate();  // ทำลาย sesion
        return "success";
    }

}
