package Action;

import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;

/**
 *
 * @author Top_Sumeta
 */
public class IndexAction extends ActionSupport {

    private String url;
    Connection conn;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String execute() {
        return SUCCESS;
    }

}
