package Action.Indicators;

/**
 *
 * @author top_sumeta
 */
import Model.IndicatorsModel;
import Model.PerformanceModel;
import Model.ReIndicatorsPersonnelModel;
import java.util.List;

public class FourDotThree extends Action.IndexAction {

    private int in_id;
    private IndicatorsModel indicatorsModel;
    private List list;
    private PerformanceModel performanceModel;
    private List performanceList;
    private List list1;
    private ReIndicatorsPersonnelModel reIndicatorsPersonnelModel;

    public FourDotThree() {
        indicatorsModel = new IndicatorsModel();
        performanceModel = new PerformanceModel();
        reIndicatorsPersonnelModel = new ReIndicatorsPersonnelModel();
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public IndicatorsModel getIndicatorsModel() {
        return indicatorsModel;
    }

    public void setIndicatorsModel(IndicatorsModel indicatorsModel) {
        this.indicatorsModel = indicatorsModel;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public PerformanceModel getPerformanceModel() {
        return performanceModel;
    }

    public void setPerformanceModel(PerformanceModel performanceModel) {
        this.performanceModel = performanceModel;
    }

    public List getPerformanceList() {
        return performanceList;
    }

    public void setPerformanceList(List performanceList) {
        this.performanceList = performanceList;
    }

    public List getList1() {
        return list1;
    }

    public void setList1(List list1) {
        this.list1 = list1;
    }

    public ReIndicatorsPersonnelModel getReIndicatorsPersonnelModel() {
        return reIndicatorsPersonnelModel;
    }

    public void setReIndicatorsPersonnelModel(ReIndicatorsPersonnelModel reIndicatorsPersonnelModel) {
        this.reIndicatorsPersonnelModel = reIndicatorsPersonnelModel;
    }


    
    

    public String index() {
        indicatorsModel.setIn_id(in_id);
        list = indicatorsModel.where_in_id();

        performanceModel.setIn_id(in_id);
        performanceList = performanceModel.where();

        reIndicatorsPersonnelModel.setIn_id(in_id);
        list1 = reIndicatorsPersonnelModel.where_in_id();
       

        return SUCCESS;
    }

}
