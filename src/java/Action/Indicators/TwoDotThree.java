package Action.Indicators;

/**
 *
 * @author Top_Sumeta
 *
 */
import Model.IndicatorsModel;
import java.util.List;
import Model.CalculateModel;

public class TwoDotThree extends Action.IndexAction {

    private int in_id;
    private IndicatorsModel indicatorsModel;
    private List list;
    private int el_id;
    private CalculateModel calculateModel;
    private List CalList;
    private List CalList2;

    public TwoDotThree() {
        indicatorsModel = new IndicatorsModel();
        calculateModel = new CalculateModel();
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public IndicatorsModel getIndicatorsModel() {
        return indicatorsModel;
    }

    public void setIndicatorsModel(IndicatorsModel indicatorsModel) {
        this.indicatorsModel = indicatorsModel;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getEl_id() {
        return el_id;
    }

    public void setEl_id(int el_id) {
        this.el_id = el_id;
    }

    public CalculateModel getCalculateModel() {
        return calculateModel;
    }

    public void setCalculateModel(CalculateModel calculateModel) {
        this.calculateModel = calculateModel;
    }

    public List getCalList() {
        return CalList;
    }

    public void setCalList(List CalList) {
        this.CalList = CalList;
    }

    public List getCalList2() {
        return CalList2;
    }

    public void setCalList2(List CalList2) {
        this.CalList2 = CalList2;
    }

    public String index() {
        indicatorsModel.setIn_id(in_id);
        list = indicatorsModel.where_in_id();
        indicatorsModel = indicatorsModel.find(in_id);

        calculateModel.setIn_id(in_id);
        CalList = calculateModel.list1();
        CalList2 = calculateModel.list2();

        return SUCCESS;

    }

    public String save() {

        if (in_id == 0) {
            indicatorsModel.save(indicatorsModel);
        } else {
            indicatorsModel.update(indicatorsModel, in_id);
            setIn_id(0);
        }
        list = indicatorsModel.where();
        el_id = indicatorsModel.getEl_id();
        return SUCCESS;
    }

    public String edit() {
        indicatorsModel = indicatorsModel.find(in_id);

        return SUCCESS;
    }

    public String delete() {

        return SUCCESS;
    }

}
