package Action;

/**
 *
 * @author Top_Sumeta
 */
import java.util.List;
import Model.PerformanceModel;
import Model.AttachmentsModel;

public class PerformanceAction extends IndexAction {

    private List list;
    private PerformanceModel performanceModel;
    private int pe_id;
    private int in_id;
    private AttachmentsModel attachmentsModel;
    private List listAtt;
    

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public PerformanceModel getPerformanceModel() {
        return performanceModel;
    }

    public void setPerformanceModel(PerformanceModel performanceModel) {
        this.performanceModel = performanceModel;
    }

    public int getPe_id() {
        return pe_id;
    }

    public void setPe_id(int pe_id) {
        this.pe_id = pe_id;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public AttachmentsModel getAttachmentsModel() {
        return attachmentsModel;
    }

    public void setAttachmentsModel(AttachmentsModel attachmentsModel) {
        this.attachmentsModel = attachmentsModel;
    }

    public List getListAtt() {
        return listAtt;
    }

    public void setListAtt(List listAtt) {
        this.listAtt = listAtt;
    }
    
    
    public PerformanceAction() {
        performanceModel = new PerformanceModel();
        attachmentsModel = new AttachmentsModel();
    }

    public String index() {
        performanceModel.setIn_id(in_id);
        list = performanceModel.where();
        return SUCCESS;
    }

    public String save() {
        if (pe_id == 0) {
            performanceModel.save(performanceModel);
        } else {
            performanceModel.update(performanceModel, pe_id);
            setPe_id(0);
        }
        list = performanceModel.where();
        in_id = performanceModel.getIn_id();
        return SUCCESS;
    }

    public String edit() {
        performanceModel = performanceModel.find(pe_id);
        return SUCCESS;
    }

    public String delete() {
        performanceModel.delete(pe_id);
        in_id = getIn_id();
        return SUCCESS;
    }
    
    public String detail(){
        performanceModel.setPe_id(pe_id);
        list = performanceModel.where_pe_id();
        attachmentsModel.setPe_id(pe_id);
        listAtt = attachmentsModel.where();
        
    return SUCCESS;
    }

}
