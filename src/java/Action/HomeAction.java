
package Action;

import java.util.List;
import Model.YearModel;
import Model.ElementsModel;
/**
 * @author Top_Sumeta <tpsumeta@gmail.com>
 */
public class HomeAction  extends IndexAction{

   private List YearList;
    private YearModel yearModel;
    private int y_id;
    private ElementsModel elementsModel;

    public HomeAction() {
        yearModel = new YearModel();
        elementsModel = new ElementsModel();
    }

    public List getYearList() {
        return YearList;
    }

    public void setYearList(List YearList) {
        this.YearList = YearList;
    }

    public YearModel getYearModel() {
        return yearModel;
    }

    public void setYearModel(YearModel yearModel) {
        this.yearModel = yearModel;
    }

    public int getY_id() {
        return y_id;
    }

    public void setY_id(int y_id) {
        this.y_id = y_id;
    }

    public String index() {
        YearList = yearModel.list();
        return SUCCESS;
    }
    public String index2() {
        // YearList = yearModel.list();
       elementsModel.setY_id(y_id);
        YearList = elementsModel.where();
        
        return SUCCESS;
    }

}
