package Action;
/**
 *
 * @author dev
 */
import java.util.List;
import Model.AttachmentsModel;
import Model.ProsonnelModel;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;

public class AttachmentsAction extends IndexAction implements ServletRequestAware {

    private List attList;
    private List PList;
    private AttachmentsModel attachmentsModel;
    private  ProsonnelModel prosonnelModel;
    private int at_id;
    private HttpServletRequest servletRequest;
    private File at_name;
    private String at_nameFileName;
    private int p_id;
    private int pe_id;

    public List getAttList() {
        return attList;
    }

    public void setAttList(List attList) {
        this.attList = attList;
    }

    public AttachmentsModel getAttachmentsModel() {
        return attachmentsModel;
    }

    public void setAttachmentsModel(AttachmentsModel attachmentsModel) {
        this.attachmentsModel = attachmentsModel;
    }

    public List getPList() {
        return PList;
    }

    public void setPList(List PList) {
        this.PList = PList;
    }

    public ProsonnelModel getProsonnelModel() {
        return prosonnelModel;
    }

    public void setProsonnelModel(ProsonnelModel prosonnelModel) {
        this.prosonnelModel = prosonnelModel;
    }

    public int getAt_id() {
        return at_id;
    }

    public void setAt_id(int at_id) {
        this.at_id = at_id;
    }

    public File getAt_name() {
        return at_name;
    }

    public void setAt_name(File at_name) {
        this.at_name = at_name;
    }

    public String getAt_nameFileName() {
        return at_nameFileName;
    }

    public void setAt_nameFileName(String at_nameFileName) {
        this.at_nameFileName = at_nameFileName;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public int getPe_id() {
        return pe_id;
    }

    public void setPe_id(int pe_id) {
        this.pe_id = pe_id;
    }
    
    
    

    public AttachmentsAction() {
        attachmentsModel = new AttachmentsModel();
//        prosonnelModel = new ProsonnelModel();
        
    }

    public String index() {
        attList = attachmentsModel.list();       
        return SUCCESS;
    }
    
        public String join() {
        attList = attachmentsModel.join();
        return SUCCESS;
    }

    public String save() throws IOException {

        if (getAt_name() != null) {
            String filePath = servletRequest.getRealPath("/image/");
            File destinationFie = new File(filePath, at_nameFileName);
            FileUtils.copyFile(at_name, destinationFie);
        }

        if (at_id == 0) {
            attachmentsModel.setAt_link(at_nameFileName);
            attachmentsModel.setAt_name(at_nameFileName);
            attachmentsModel.save(attachmentsModel);
        } else {
            attachmentsModel.update(attachmentsModel, at_id);
            setAt_id(at_id);
        }

        return SUCCESS;
    }

    public String edit() {
        attachmentsModel = attachmentsModel.find(at_id);
        attList = attachmentsModel.list();
        return SUCCESS;
    }

    public String delete() {
        attachmentsModel.delete(at_id);
        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

}
