package Action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import javax.servlet.http.HttpServletRequest;
//import net.sf.jasperreports.engine.JREmptyDataSource;
import org.apache.struts2.interceptor.ServletRequestAware;

public class ReportAction extends IndexAction implements ServletRequestAware {

    Connection connect;
    private HttpServletRequest servletRequest;
    private int in_id;
    private int y_id;
    // set database
      protected String db_name = "jdbc:mysql://localhost/db_sdu_sar";
//    protected String db_name = "jdbc:mysql://mysql83051-sumeta.j.layershift.co.uk/db_sdu_sar";
    protected String user = "root";
    protected String password = "";
//    protected String password = "KqL9g2aCLm";

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public int getY_id() {
        return y_id;
    }

    public void setY_id(int y_id) {
        this.y_id = y_id;
    }

    public String index() {
        return SUCCESS;
    }

    public String ireport() throws FileNotFoundException, JRException, IOException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(db_name + "?user=" + user + "&password=" + password);
            String path = servletRequest.getRealPath("/");
            InputStream input = new FileInputStream(new File(path + "/report/indicators.jrxml"));
            JasperDesign design = JRXmlLoader.load(input);
            JasperReport report = JasperCompileManager.compileReport(design);

            Map parameters = new HashMap();
            parameters.put("ReportTitle", "PDF JasperReport");
            parameters.put("in_id", in_id);

//        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());  // กรณีไม่ต่อ database
            JasperPrint print = JasperFillManager.fillReport(report, parameters, connect);
            OutputStream output = new FileOutputStream(new File(path + "/report/indicators.pdf")); //exportเป็นไฟล์
            JasperExportManager.exportReportToPdfStream(print, output);

        } catch (SQLException ex) {
            Logger.getLogger(ReportAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return SUCCESS;
    }

    public String TwoDotTwo() throws FileNotFoundException, JRException, IOException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(db_name + "?user=" + user + "&password=" + password);
            String path = servletRequest.getRealPath("/");
            InputStream input = new FileInputStream(new File(path + "/report/2.2.jrxml"));
            JasperDesign design = JRXmlLoader.load(input);
            JasperReport report = JasperCompileManager.compileReport(design);

            Map parameters = new HashMap();
            parameters.put("ReportTitle", "PDF JasperReport");
            parameters.put("in_id", in_id);

//        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());  // กรณีไม่ต่อ database
            JasperPrint print = JasperFillManager.fillReport(report, parameters, connect);
            OutputStream output = new FileOutputStream(new File(path + "/report/2.2.pdf")); //exportเป็นไฟล์
            JasperExportManager.exportReportToPdfStream(print, output);

        } catch (SQLException ex) {
            Logger.getLogger(ReportAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return SUCCESS;
    }

    public String TwoDotThree() throws FileNotFoundException, JRException, IOException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(db_name + "?user=" + user + "&password=" + password);
            String path = servletRequest.getRealPath("/");
            InputStream input = new FileInputStream(new File(path + "/report/2.3.jrxml"));
            JasperDesign design = JRXmlLoader.load(input);
            JasperReport report = JasperCompileManager.compileReport(design);

            Map parameters = new HashMap();
            parameters.put("ReportTitle", "PDF JasperReport");
            parameters.put("in_id", in_id);

//        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());  // กรณีไม่ต่อ database
            JasperPrint print = JasperFillManager.fillReport(report, parameters, connect);
            OutputStream output = new FileOutputStream(new File(path + "/report/2.3.pdf")); //exportเป็นไฟล์
            JasperExportManager.exportReportToPdfStream(print, output);

        } catch (SQLException ex) {
            Logger.getLogger(ReportAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return SUCCESS;
    }

    public String FourDotThree() throws FileNotFoundException, JRException, IOException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(db_name + "?user=" + user + "&password=" + password);
            String path = servletRequest.getRealPath("/");
            InputStream input = new FileInputStream(new File(path + "/report/4.3.jrxml"));
            JasperDesign design = JRXmlLoader.load(input);
            JasperReport report = JasperCompileManager.compileReport(design);

            Map parameters = new HashMap();
            parameters.put("ReportTitle", "PDF JasperReport");
            parameters.put("in_id", in_id);

//        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());  // กรณีไม่ต่อ database
            JasperPrint print = JasperFillManager.fillReport(report, parameters, connect);
            OutputStream output = new FileOutputStream(new File(path + "/report/4.3.pdf")); //exportเป็นไฟล์
            JasperExportManager.exportReportToPdfStream(print, output);

        } catch (SQLException ex) {
            Logger.getLogger(ReportAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return SUCCESS;
    }

    public String Summary() throws FileNotFoundException, JRException, IOException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(db_name + "?user=" + user + "&password=" + password);
            String path = servletRequest.getRealPath("/");
            InputStream input = new FileInputStream(new File(path + "/report/summary.jrxml"));
            JasperDesign design = JRXmlLoader.load(input);
            JasperReport report = JasperCompileManager.compileReport(design);

            Map parameters = new HashMap();
            parameters.put("ReportTitle", "PDF JasperReport");
            parameters.put("y_id", y_id);

//        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());  // กรณีไม่ต่อ database
            JasperPrint print = JasperFillManager.fillReport(report, parameters, connect);
            OutputStream output = new FileOutputStream(new File(path + "/report/summary.pdf")); //exportเป็นไฟล์
            JasperExportManager.exportReportToPdfStream(print, output);

        } catch (SQLException ex) {
            Logger.getLogger(ReportAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

}
