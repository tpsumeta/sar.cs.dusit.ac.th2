package Action;

/**
 * @author Top_Sumeta Element องค์ประกอบ
 */
import Model.ElementsModel;
//import Model.IndicatorsModel;
import java.util.List;

public class ElementsAction extends IndexAction {

    private ElementsModel elementsModel;
    private List elementsList;
    private int y_id;
    private int el_id;
    private int  el_time;
    private String el_name;
//    private IndicatorsModel indicatorsModel;

    public ElementsAction() {
        elementsModel = new ElementsModel();
//        indicatorsModel = new IndicatorsModel();
    }

    public ElementsModel getElementsModel() {
        return elementsModel;
    }

    public void setElementsModel(ElementsModel elementsModel) {
        this.elementsModel = elementsModel;
    }

    public List getElementsList() {
        return elementsList;
    }

    public void setElementsList(List elementsList) {
        this.elementsList = elementsList;
    }

    public int getY_id() {
        return y_id;
    }

    public void setY_id(int y_id) {
        this.y_id = y_id;
    }

    public int getEl_id() {
        return el_id;
    }

    public void setEl_id(int el_id) {
        this.el_id = el_id;
    }

    public int getEl_time() {
        return el_time;
    }

    public void setEl_time(int el_time) {
        this.el_time = el_time;
    }

    public String getEl_name() {
        return el_name;
    }

    public void setEl_name(String el_name) {
        this.el_name = el_name;
    }

    
    public String index() {
        elementsModel.setY_id(y_id);
        elementsList = elementsModel.where();
        return SUCCESS;
    }

    public String save() {
        if (el_id == 0) {

            elementsModel.save(elementsModel);
        } else {
            elementsModel.update(elementsModel, el_id);
            setEl_id(0);
        }
        elementsList = elementsModel.where();
        y_id= elementsModel.getY_id();
        return SUCCESS;
    }

    public String save_json(){
        elementsModel.save(elementsModel);
        return SUCCESS;
    }

    public String edit() {
        elementsModel = elementsModel.find(el_id);
        return SUCCESS;
    }

    public String delete() {
        elementsModel.delete(el_id);
        index();
        return SUCCESS;
    }
    
    public  String gen(){
        elementsModel.setY_id(1);
       elementsList = elementsModel.where();
       return SUCCESS;
   }

}
