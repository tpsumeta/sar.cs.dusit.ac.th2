package Action;

/**
 *
 * @author dev
 */
import Model.ReIndicatorsPersonnelModel;

public class ReIndicatorsPersonnelAction extends IndexAction {

    private int ip_id;
    private int ip_position;
    private int in_id;
    private ReIndicatorsPersonnelModel reIndicatorsPersonnelModel;

    public ReIndicatorsPersonnelAction() {
        reIndicatorsPersonnelModel = new ReIndicatorsPersonnelModel();
    }

    public int getIp_id() {
        return ip_id;
    }

    public void setIp_id(int ip_id) {
        this.ip_id = ip_id;
    }

    public int getIp_position() {
        return ip_position;
    }

    public void setIp_position(int ip_position) {
        this.ip_position = ip_position;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public ReIndicatorsPersonnelModel getReIndicatorsPersonnelModel() {
        return reIndicatorsPersonnelModel;
    }

    public void setReIndicatorsPersonnelModel(ReIndicatorsPersonnelModel reIndicatorsPersonnelModel) {
        this.reIndicatorsPersonnelModel = reIndicatorsPersonnelModel;
    }



    public String save() {
        if (ip_id == 0) {
            reIndicatorsPersonnelModel.save(reIndicatorsPersonnelModel);
        } else {
            reIndicatorsPersonnelModel.update(reIndicatorsPersonnelModel, ip_id);
            setIp_id(0);
        }

        return SUCCESS;
    }
    
    public String delete(){
     reIndicatorsPersonnelModel.delete(ip_id);
    return NONE;
    }

}
