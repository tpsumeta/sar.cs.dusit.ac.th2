package Action;

import java.applet.Applet;
import Model.CriteriaModel;
import java.util.List;

public class CriteriaAction extends IndexAction {

    private List list;
    private int cr_id;
    private CriteriaModel criteriaModel;
    private int in_id;

    public CriteriaAction() {
        criteriaModel = new CriteriaModel();
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getCr_id() {
        return cr_id;
    }

    public void setCr_id(int cr_id) {
        this.cr_id = cr_id;
    }

    public CriteriaModel getCriteriaModel() {
        return criteriaModel;
    }

    public void setCriteriaModel(CriteriaModel criteriaModel) {
        this.criteriaModel = criteriaModel;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public String index() {
        criteriaModel.setIn_id(in_id);
        list = criteriaModel.list();
        return SUCCESS;
    }

    public String edit() {
        criteriaModel = criteriaModel.find(cr_id);
        return SUCCESS;
    }

    public String save() {
        if (cr_id == 0) {
            criteriaModel.save(criteriaModel);
        } else {
            criteriaModel.update(criteriaModel, cr_id);
            setCr_id(0);
        }
        in_id = criteriaModel.getIn_id();
        list = criteriaModel.list();

        return SUCCESS;
    }

    public String delete() {
        criteriaModel.delete(cr_id);;
        criteriaModel.setIn_id(in_id);
        list = criteriaModel.list();
        return SUCCESS;
    }

}
