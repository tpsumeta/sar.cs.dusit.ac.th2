package Action;

import Model.CalculateModel;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;

public class CalculateAction extends IndexAction implements ServletRequestAware{

    private List list;
    private CalculateModel calculateModel;
    private int ca_id;
    private int in_id;
    private int no;
    private File ca_comment;
    private String ca_commentFileName;
    private HttpServletRequest servletRequest;

    public CalculateAction() {
        calculateModel = new CalculateModel();
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public CalculateModel getCalculateModel() {
        return calculateModel;
    }

    public void setCalculateModel(CalculateModel calculateModel) {
        this.calculateModel = calculateModel;
    }

    public int getCa_id() {
        return ca_id;
    }

    public void setCa_id(int ca_id) {
        this.ca_id = ca_id;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public File getCa_comment() {
        return ca_comment;
    }

    public void setCa_comment(File ca_comment) {
        this.ca_comment = ca_comment;
    }

    public String getCa_commentFileName() {
        return ca_commentFileName;
    }

    public void setCa_commentFileName(String ca_commentFileName) {
        this.ca_commentFileName = ca_commentFileName;
    }
    
    
    

    public String index() {
        calculateModel.setIn_id(in_id);
//        list = calculateModel.list();
        return SUCCESS;
    }

    
     public String edit() {
        calculateModel = calculateModel.find(ca_id);
        return SUCCESS;
    }

    public String save() {
        if (ca_id == 0) {
            calculateModel.setCa_comment("0");
            calculateModel.save(calculateModel);
        } else {
            calculateModel.update(calculateModel, ca_id);
            setCa_id(0);
        }
//        in_id = criteriaModel.getIn_id();
//        list = criteriaModel.list();

        return SUCCESS;
    }
    
    public String delete(){
       calculateModel.delete(ca_id);
       
       return SUCCESS;
    }

    
       public String saveatt() throws IOException {
              if (getCa_comment() != null) {
            String filePath = servletRequest.getRealPath("/image/");
            File destinationFie = new File(filePath, ca_commentFileName);
            FileUtils.copyFile(ca_comment, destinationFie);
        }
        if (ca_id == 0) {
            calculateModel.setCa_name("file");
            calculateModel.setCa_link(ca_commentFileName);
            calculateModel.setCa_comment(ca_commentFileName);
            calculateModel.save(calculateModel);
        } else {
            calculateModel.update(calculateModel, ca_id);
            setCa_id(0);
        }
        return SUCCESS;
       }
       
          @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }
    
}
