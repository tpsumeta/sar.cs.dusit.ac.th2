package Action;

/**
 *
 * @author Top_Sumeta
 * @contac tpsumeta@gmail.com
 */
import java.util.List;
import Model.IndicatorsModel;
import Model.PerformanceModel;
import Model.ReIndicatorsPersonnelModel;
import java.util.ArrayList;
import Model.CriteriaModel;

public class IndicatorsAction extends IndexAction {

    private List list;
    private IndicatorsModel indicatorsModel;
    private int in_id;
    private int el_id;
    private List performanceList;
    private PerformanceModel performanceModel;
    private List listType;
    private String in_time;
    private ReIndicatorsPersonnelModel reIndicatorsPersonnelModel;
    private List list1;
    private CriteriaModel criteriaModel;
    private List CrList;
    private List ScoreList;

    public IndicatorsAction() {
        indicatorsModel = new IndicatorsModel();
        performanceModel = new PerformanceModel();
        reIndicatorsPersonnelModel = new ReIndicatorsPersonnelModel();

        listType = new ArrayList();
        listType = indicatorsModel.type();

        criteriaModel = new CriteriaModel();

    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public IndicatorsModel getIndicatorsModel() {
        return indicatorsModel;
    }

    public void setIndicatorsModel(IndicatorsModel indicatorsModel) {
        this.indicatorsModel = indicatorsModel;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public int getEl_id() {
        return el_id;
    }

    public void setEl_id(int el_id) {
        this.el_id = el_id;
    }

    public List getPerformanceList() {
        return performanceList;
    }

    public void setPerformanceList(List performanceList) {
        this.performanceList = performanceList;
    }

    public PerformanceModel getPerformanceModel() {
        return performanceModel;
    }

    public void setPerformanceModel(PerformanceModel performanceModel) {
        this.performanceModel = performanceModel;
    }

    public List getListType() {
        return listType;
    }

    public void setListType(List listType) {
        this.listType = listType;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public ReIndicatorsPersonnelModel getReIndicatorsPersonnelModel() {
        return reIndicatorsPersonnelModel;
    }

    public void setReIndicatorsPersonnelModel(ReIndicatorsPersonnelModel reIndicatorsPersonnelModel) {
        this.reIndicatorsPersonnelModel = reIndicatorsPersonnelModel;
    }

    public List getList1() {
        return list1;
    }

    public void setList1(List list1) {
        this.list1 = list1;
    }

    public CriteriaModel getCriteriaModel() {
        return criteriaModel;
    }

    public void setCriteriaModel(CriteriaModel criteriaModel) {
        this.criteriaModel = criteriaModel;
    }

    public List getCrList() {
        return CrList;
    }

    public void setCrList(List CrList) {
        this.CrList = CrList;
    }

    public List getScoreList() {
        return ScoreList;
    }

    public void setScoreList(List ScoreList) {
        this.ScoreList = ScoreList;
    }

    public String index() {
        indicatorsModel.setEl_id(el_id);
        list = indicatorsModel.where();
        return SUCCESS;
    }
    
    
    // รายละเอียดของตัวบ่งชี้ IndicatorsDetail
    public String detail() {
        
        indicatorsModel.setIn_id(in_id);
        list = indicatorsModel.where_in_id();

        performanceModel.setIn_id(in_id);
        performanceList = performanceModel.where();

        reIndicatorsPersonnelModel.setIn_id(in_id);
        list1 = reIndicatorsPersonnelModel.where_in_id();

        criteriaModel.setIn_id(in_id);
        CrList = criteriaModel.list_group_by();
        ScoreList = criteriaModel.list();

        /**
         * การกำหนดสิทธิการเข้าสถึง ตัวบ่งชี้ ดึงข้อมูลการเข้าถึงมาจาก db
         * re_indicators_personnel ส่งกลับค่า เป็น true หรือ false ถ้าเป็น true
         * แสดงว่าสามารถจัดการตั่งบ่งชี้นี้ได้ ถ้าเป็น false
         * แสดงว่าไม่มีสิทธิจัดการตัวบ่งชี้นี้ได้
         */
        
        String login;
        login = "true";
        if (login == "true") {
            //   return ERROR;
            return SUCCESS;
        } else {
            //  return ERROR;
            return SUCCESS;
        }

    }

    public String save() {
        if (in_id == 0) {
            indicatorsModel.save(indicatorsModel);
        } else {
            indicatorsModel.update(indicatorsModel, in_id);
            setIn_id(0);
        }
        list = indicatorsModel.where();
        el_id = indicatorsModel.getEl_id();
        return SUCCESS;
    }

    public String edit() {
        indicatorsModel = indicatorsModel.find(in_id);
        return SUCCESS;
    }

    public String delete() {
        indicatorsModel.delete(in_id);
        indicatorsModel.setEl_id(el_id);
        list = indicatorsModel.where();
        return SUCCESS;
    }

}
