package Model;

import static Model.IndexModel.factory;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Top Sumeta <tpsumeta@gmail.com>
 * 
 */
public class YearModel extends IndexModel {

    private int y_id;
    private String y_name;
    private int y_time;

    public int getY_id() {
        return y_id;
    }

    public void setY_id(int y_id) {
        this.y_id = y_id;
    }

    public String getY_name() {
        return y_name;
    }

    public void setY_name(String y_name) {
        this.y_name = y_name;
    }

    public int getY_time() {
        return y_time;
    }

    public void setY_time(int y_time) {
        this.y_time = y_time;
    }

    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria year = session.createCriteria(YearModel.class);
        year.addOrder(Order.desc("y_name"));
        year.addOrder(Order.desc("y_time"));
        return year.list();
    }

    public List where() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM YearModel WHERE y_id=:y_id");
        query.setParameter("y_id", getY_id());
        List year = query.list();
        session.getTransaction().commit();
        session.close();
        return year;
    }

    public YearModel save(YearModel yearModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(yearModel);
        session.getTransaction().commit();
        return yearModel;
    }

    public YearModel find(int y_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        YearModel yearModel = (YearModel) session.get(YearModel.class, y_id);
        session.getTransaction().commit();
        session.close();
        return yearModel;
    }

    public void update(YearModel yearModel, int y_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        yearModel.setY_id(y_id);
        session.update(yearModel);
        session.getTransaction().commit();
    }

}
