package Model;

/**
 * ตารางตัวบ่งชี้  sar_indicators
 * @author Top_Sumeta ; emial : tpsumeta@gmail.com
 * 
 */
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.ArrayList;

public class IndicatorsModel extends IndexModel {

    private int in_id;              //  pk ของตาราง
    private String in_category;
    private String in_time;
    private String in_name;
    private String in_type;
    private String in_good;  
    private String in_development;
    private String in_practices;
    private int el_id;
    private int in_target;
    private int in_numerator;
    private int in_divisor;
    private double in_num_performance;
    private float in_score;
    private int in_achieve;
     private String in_criteria;
    private double in_percent_add;
    private double percent_old;

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public String getIn_category() {
        return in_category;
    }

    public void setIn_category(String in_category) {
        this.in_category = in_category;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getIn_name() {
        return in_name;
    }

    public void setIn_name(String in_name) {
        this.in_name = in_name;
    }

    public String getIn_type() {
        return in_type;
    }

    public void setIn_type(String in_type) {
        this.in_type = in_type;
    }

    public String getIn_good() {
        return in_good;
    }

    public void setIn_good(String in_good) {
        this.in_good = in_good;
    }

    public String getIn_development() {
        return in_development;
    }

    public void setIn_development(String in_development) {
        this.in_development = in_development;
    }

    public String getIn_practices() {
        return in_practices;
    }

    public void setIn_practices(String in_practices) {
        this.in_practices = in_practices;
    }
    
    

    public int getEl_id() {
        return el_id;
    }

    public void setEl_id(int el_id) {
        this.el_id = el_id;
    }

    public int getIn_target() {
        return in_target;
    }

    public void setIn_target(int in_target) {
        this.in_target = in_target;
    }

    public int getIn_numerator() {
        return in_numerator;
    }

    public void setIn_numerator(int in_numerator) {
        this.in_numerator = in_numerator;
    }

    public int getIn_divisor() {
        return in_divisor;
    }

    public void setIn_divisor(int in_divisor) {
        this.in_divisor = in_divisor;
    }

    public double getIn_num_performance() {
        return in_num_performance;
    }

    public void setIn_num_performance(double in_num_performance) {
        this.in_num_performance = in_num_performance;
    }
    
    
    public float getIn_score() {
        return in_score;
    }

    public void setIn_score(float in_score) {
        this.in_score = in_score;
    }

    public int getIn_achieve() {
        return in_achieve;
    }

    public void setIn_achieve(int in_achieve) {
        this.in_achieve = in_achieve;
    }

    public String getIn_criteria() {
        return in_criteria;
    }

    public void setIn_criteria(String in_criteria) {
        this.in_criteria = in_criteria;
    }

    
    public double getIn_percent_add() {
        return in_percent_add;
    }

    public void setIn_percent_add(double in_percent_add) {
        this.in_percent_add = in_percent_add;
    }

    public double getPercent_old() {
        return percent_old;
    }

    public void setPercent_old(double percent_old) {
        this.percent_old = percent_old;
    }
    
    

    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria indicators = session.createCriteria(IndicatorsModel.class);
        return indicators.list();
    }

    public List where() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM IndicatorsModel WHERE el_id=:el_id");
        query.setParameter("el_id", getEl_id());
        List indicators = query.list();

        session.getTransaction().commit();
        session.close();
        return indicators;
    }

    public List where_in_id() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM IndicatorsModel WHERE in_id=:in_id");
        query.setParameter("in_id", getIn_id());
        List indicators = query.list();

        session.getTransaction().commit();
        session.close();
        return indicators;
    }

    public IndicatorsModel save(IndicatorsModel indicatorsModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(indicatorsModel);
        session.getTransaction().commit();
        return indicatorsModel;
    }

    public IndicatorsModel find(int in_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        IndicatorsModel indicatorsModel = (IndicatorsModel) session.get(IndicatorsModel.class, in_id);
        session.getTransaction().commit();
        session.close();
        return indicatorsModel;
    }

    public void update(IndicatorsModel indicatorsModel, int in_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        indicatorsModel.setIn_id(in_id);
        session.update(indicatorsModel);
        session.getTransaction().commit();
    }

    public void delete(int in_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        IndicatorsModel indicatorsModel = (IndicatorsModel) session.get(IndicatorsModel.class, in_id);
        session.delete(indicatorsModel);
        session.getTransaction().commit();
        session.close();
    }

    public ArrayList type() {
        ArrayList type = new ArrayList();
        type.add("กระบวนการ");
        type.add("ข้อมูลนำเข้า");
        type.add("ผลผลิต");
        return type;
    }

}
