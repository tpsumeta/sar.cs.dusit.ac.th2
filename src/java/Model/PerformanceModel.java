package Model;

/**
 *
 * @author dev ผลการดำเนินงาน
 */
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

public class PerformanceModel extends IndexModel {

    private int pe_id;
    private int pe_have;
    private int pe_point;
    private String p_standard;
    private String p_performance;
    private int in_id;
    private String pe_data;
    private int pe_teacher;
    private double pe_monny;
    private double pe_monny_avg;
    private double pe_score;
    private double pe_weight;
    

    public int getPe_id() {
        return pe_id;
    }

    public void setPe_id(int pe_id) {
        this.pe_id = pe_id;
    }

    public int getPe_have() {
        return pe_have;
    }

    public void setPe_have(int pe_have) {
        this.pe_have = pe_have;
    }

    public int getPe_point() {
        return pe_point;
    }

    public void setPe_point(int pe_point) {
        this.pe_point = pe_point;
    }

    public String getP_standard() {
        return p_standard;
    }

    public void setP_standard(String p_standard) {
        this.p_standard = p_standard;
    }

    public String getP_performance() {
        return p_performance;
    }

    public void setP_performance(String p_performance) {
        this.p_performance = p_performance;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public String getPe_data() {
        return pe_data;
    }

    public void setPe_data(String pe_data) {
        this.pe_data = pe_data;
    }

    public int getPe_teacher() {
        return pe_teacher;
    }

    public void setPe_teacher(int pe_teacher) {
        this.pe_teacher = pe_teacher;
    }

    public double getPe_monny() {
        return pe_monny;
    }

    public void setPe_monny(double pe_monny) {
        this.pe_monny = pe_monny;
    }

    public double getPe_monny_avg() {
        return pe_monny_avg;
    }

    public void setPe_monny_avg(double pe_monny_avg) {
        this.pe_monny_avg = pe_monny_avg;
    }

    public double getPe_score() {
        return pe_score;
    }

    public void setPe_score(double pe_score) {
        this.pe_score = pe_score;
    }

    public double getPe_weight() {
        return pe_weight;
    }

    public void setPe_weight(double pe_weight) {
        this.pe_weight = pe_weight;
    }

    


    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(PerformanceModel.class);
        return criteria.list();
    }

    public List where() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM PerformanceModel WHERE in_id=:in_id order by pe_point asc");
        query.setParameter("in_id", getIn_id());
        
        List performance = query.list();

        session.getTransaction().commit();
        session.close();
        return performance;
    }

    public List where_pe_id() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM PerformanceModel WHERE pe_id=:pe_id");
        query.setParameter("pe_id", getPe_id());
        List performance = query.list();
        session.getTransaction().commit();
        session.close();
        return performance;
    }

    public PerformanceModel save(PerformanceModel performanceModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(performanceModel);
        session.getTransaction().commit();
        return performanceModel;
    }

    public PerformanceModel find(int pe_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        PerformanceModel performanceModel = (PerformanceModel) session.get(PerformanceModel.class, pe_id);
        session.getTransaction().commit();
        session.close();
        return performanceModel;
    }

    public void update(PerformanceModel performanceModel, int pe_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        performanceModel.setPe_id(pe_id);
        session.update(performanceModel);
        session.getTransaction().commit();
    }

    public void delete(int in_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        PerformanceModel performanceModel = (PerformanceModel) session.get(PerformanceModel.class, in_id);
        session.delete(performanceModel);
        session.getTransaction().commit();
        session.close();
    }

}
