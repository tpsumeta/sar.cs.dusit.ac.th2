package Model;

/**
 *
 * @author Top_Sumeta ไฟล์แนบ
 */
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

public class AttachmentsModel extends IndexModel {

    private int at_id;
    private String at_name;
    private String at_link;
    private int p_id;
    private int pe_id;

    public int getAt_id() {
        return at_id;
    }

    public void setAt_id(int at_id) {
        this.at_id = at_id;
    }

    public String getAt_name() {
        return at_name;
    }

    public void setAt_name(String at_name) {
        this.at_name = at_name;
    }

    public String getAt_link() {
        return at_link;
    }

    public void setAt_link(String at_link) {
        this.at_link = at_link;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public int getPe_id() {
        return pe_id;
    }

    public void setPe_id(int pe_id) {
        this.pe_id = pe_id;
    }

    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(AttachmentsModel.class);
        return criteria.list();
    }

    public List where() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM AttachmentsModel WHERE pe_id=:pe_id");
        query.setParameter("pe_id", getPe_id());
        List attList = query.list();
        session.getTransaction().commit();
        session.close();
        return attList;
    }

    public List join() {
        Session session = factory.openSession();
        session.beginTransaction();

        Query query = session.createQuery("FROM AttachmentsModel");
        List prosonnel = query.list();

        session.getTransaction().commit();
        session.close();
        return prosonnel;
    }

    public AttachmentsModel save(AttachmentsModel attachmentsModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(attachmentsModel);
        session.getTransaction().commit();
        return attachmentsModel;
    }

    public AttachmentsModel find(int at_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        AttachmentsModel attachmentsModel = (AttachmentsModel) session.get(AttachmentsModel.class, at_id);
        session.getTransaction().commit();
        session.close();
        return attachmentsModel;
    }

    public void update(AttachmentsModel attachmentsModel, int at_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        attachmentsModel.setP_id(at_id);
        session.update(attachmentsModel);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(int at_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        AttachmentsModel attachmentsModel = (AttachmentsModel) session.get(AttachmentsModel.class, at_id);
        session.delete(attachmentsModel);
        session.getTransaction().commit();
        session.close();
    }

}
