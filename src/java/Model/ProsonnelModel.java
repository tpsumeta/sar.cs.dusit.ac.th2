package Model;

import java.sql.Array;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class ProsonnelModel extends IndexModel {

    private int p_id;
    private String p_name;
    private String p_tel;
    private String p_email;
    private String p_idcard;
    private String p_img;
    private String p_user;
    private String p_pass;
    private String p_status;
    private int p_active;

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_tel() {
        return p_tel;
    }

    public void setP_tel(String p_tel) {
        this.p_tel = p_tel;
    }

    public String getP_email() {
        return p_email;
    }

    public void setP_email(String p_email) {
        this.p_email = p_email;
    }

    public String getP_idcard() {
        return p_idcard;
    }

    public void setP_idcard(String p_idcard) {
        this.p_idcard = p_idcard;
    }

    public String getP_img() {
        return p_img;
    }

    public void setP_img(String p_img) {
        this.p_img = p_img;
    }

    public String getP_user() {
        return p_user;
    }

    public void setP_user(String p_user) {
        this.p_user = p_user;
    }

    public String getP_pass() {
        return p_pass;
    }

    public void setP_pass(String p_pass) {
        this.p_pass = p_pass;
    }

    public String getP_status() {
        return p_status;
    }

    public void setP_status(String p_status) {
        this.p_status = p_status;
    }

    public int getP_active() {
        return p_active;
    }

    public void setP_active(int p_active) {
        this.p_active = p_active;
    }

    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ProsonnelModel");
        List list = query.list();
        session.getTransaction().commit();
        session.close();
        return list;
    }

    public List where() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ProsonnelModel WHERE p_id=:id");
        query.setParameter("id", getP_id());
        List prosonnel = query.list();

        session.getTransaction().commit();
        session.close();
        return prosonnel;
    }
    
    public List login() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ProsonnelModel WHERE p_user=:user AND p_pass=:pass" );
        query.setMaxResults(1);
        query.setParameter("user", getP_user());
        query.setParameter("pass", getP_pass());
        List prosonnel = query.list();
        session.getTransaction().commit();
        session.close();
        return prosonnel;
    }
    
    

    public List active() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ProsonnelModel WHERE p_active=:p_active");
        query.setParameter("p_active", getP_active());
        List prosonnel = query.list();

        session.getTransaction().commit();
        session.close();
        return prosonnel;
    }

    public ProsonnelModel save(ProsonnelModel prosonnelModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(prosonnelModel);
        session.getTransaction().commit();
        return prosonnelModel;
    }

    public ProsonnelModel find(int p_id) {
        Session session = factory.openSession();
        session.beginTransaction();

        ProsonnelModel prosonnel = (ProsonnelModel) session.get(ProsonnelModel.class, p_id);
        session.getTransaction().commit();
        session.close();

        return prosonnel;
    }

    public void update(ProsonnelModel prosonnelModel, int p_id) {

        Session session = factory.openSession();
        session.beginTransaction();
        prosonnelModel.setP_id(p_id);
        session.update(prosonnelModel);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(int p_id) {
        Session session = factory.openSession();
        session.beginTransaction();

        ProsonnelModel prosonnel = (ProsonnelModel) session.get(ProsonnelModel.class, p_id);
        session.delete(prosonnel);

        session.getTransaction().commit();
        session.close();
    }

}
