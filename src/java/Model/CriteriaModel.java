package Model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

public class CriteriaModel extends IndexModel {

    private int cr_id;
    private int in_id;
    private int cr_score;
    private int performance;
    private String cr_text;

    public int getCr_id() {
        return cr_id;
    }

    public void setCr_id(int cr_id) {
        this.cr_id = cr_id;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public int getCr_score() {
        return cr_score;
    }

    public void setCr_score(int cr_score) {
        this.cr_score = cr_score;
    }

    public int getPerformance() {
        return performance;
    }

    public void setPerformance(int performance) {
        this.performance = performance;
    }

    public String getCr_text() {
        return cr_text;
    }

    public void setCr_text(String cr_text) {
        this.cr_text = cr_text;
    }

    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM CriteriaModel WHERE in_id=:in_id");
        query.setParameter("in_id", getIn_id());
        List list = query.list();
        return list;
    }

//    public List score() {
//        Session session = factory.openSession();
//        session.beginTransaction();
//        Query query = session.createQuery("FROM CriteriaModel WHERE in_id=:in_id");
//        List list = query.list();
//        return list;
//    }

    public List list_group_by() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM CriteriaModel WHERE in_id=:in_id group by cr_score order by cr_id asc");
        query.setParameter("in_id", getIn_id());
        List list = query.list();
        return list;
    }

    public List where() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM CriteriaModel WHERE cr_id=:cr_id");
        query.setParameter("cr_id", getCr_id());
        List list = query.list();
        return list;
    }

    public CriteriaModel save(CriteriaModel criteriaModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(criteriaModel);
        session.getTransaction().commit();
        return criteriaModel;
    }

    public CriteriaModel find(int cr_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        CriteriaModel criteriaModel = (CriteriaModel) session.get(CriteriaModel.class, cr_id);
        session.getTransaction().commit();
        session.close();
        return criteriaModel;
    }

    public void update(CriteriaModel criteriaModel, int cr_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        criteriaModel.setCr_id(cr_id);
        session.update(criteriaModel);
        session.getTransaction().commit();
    }

    public void delete(int cr_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        CriteriaModel criteriaModel = (CriteriaModel) session.get(CriteriaModel.class, cr_id);
        session.delete(criteriaModel);
        session.getTransaction().commit();
        session.close();
    }

}
