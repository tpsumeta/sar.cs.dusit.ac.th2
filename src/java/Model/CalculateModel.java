package Model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

public class CalculateModel extends IndexModel {

    private int ca_id;
    private String ca_name;
    private double ca_value;
    private String ca_comment;
    private int in_id;
    private int no;
    private String ca_link;

    public int getCa_id() {
        return ca_id;
    }

    public void setCa_id(int ca_id) {
        this.ca_id = ca_id;
    }

    public String getCa_name() {
        return ca_name;
    }

    public void setCa_name(String ca_name) {
        this.ca_name = ca_name;
    }

    public double getCa_value() {
        return ca_value;
    }

    public void setCa_value(double ca_value) {
        this.ca_value = ca_value;
    }

    public String getCa_comment() {
        return ca_comment;
    }

    public void setCa_comment(String ca_comment) {
        this.ca_comment = ca_comment;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getCa_link() {
        return ca_link;
    }

    public void setCa_link(String ca_link) {
        this.ca_link = ca_link;
    }
    
    

    public List list1() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM CalculateModel WHERE in_id=:in_id AND no =1");
        query.setParameter("in_id", getIn_id());
        List list = query.list();
        return list;
    }

    public List list2() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM CalculateModel WHERE in_id=:in_id AND no =2");
        query.setParameter("in_id", getIn_id());
        List list = query.list();
        return list;
    }

    public CalculateModel save(CalculateModel calculateModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(calculateModel);
        session.getTransaction().commit();
        return calculateModel;
    }

    public CalculateModel find(int ca_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        CalculateModel calculateModel = (CalculateModel) session.get(CalculateModel.class, ca_id);
        session.getTransaction().commit();
        session.close();
        return calculateModel;
    }

    public void update(CalculateModel calculateModel, int ca_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        calculateModel.setCa_id(ca_id);
        session.update(calculateModel);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(int ca_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        CalculateModel calculateModel = (CalculateModel) session.get(CalculateModel.class, ca_id);
        session.delete(calculateModel);
        session.getTransaction().commit();
        session.close();
    }
}
