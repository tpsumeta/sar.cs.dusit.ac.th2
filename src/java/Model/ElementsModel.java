package Model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Top_Sumeta  <tpsumeta@gmail.com>
 * 
 */
public class ElementsModel extends IndexModel {

    private int el_id;
    private String el_time;
    private String el_name;
    private int y_id;

    public int getEl_id() {
        return el_id;
    }

    public void setEl_id(int el_id) {
        this.el_id = el_id;
    }

    public String getEl_time() {
        return el_time;
    }

    public void setEl_time(String el_time) {
        this.el_time = el_time;
    }

    public String getEl_name() {
        return el_name;
    }

    public void setEl_name(String el_name) {
        this.el_name = el_name;
    }

    public int getY_id() {
        return y_id;
    }

    public void setY_id(int y_id) {
        this.y_id = y_id;
    }

    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria elements = session.createCriteria(ElementsModel.class);
        return elements.list();
    }

    public List where() {
        Session s = factory.openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ElementsModel WHERE y_id = :y_id ");
        q.setParameter("y_id", getY_id());
        List elements = q.list();
        s.getTransaction().commit();
        s.close();
        return elements;
    }
    
    public List where_join() {
        Session s = factory.openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ElementsModel WHERE y_id = :y_id ");
        q.setParameter("y_id", getY_id());
        List elements = q.list();
        s.getTransaction().commit();
        s.close();
        return elements;
    }

    public ElementsModel save(ElementsModel elementsModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(elementsModel);
        session.getTransaction().commit();
        return elementsModel;
    }

     public ElementsModel save_json(ElementsModel elementsModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(elementsModel);
        session.getTransaction().commit();
        return elementsModel;
    }

    public ElementsModel find(int el_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        ElementsModel elementsModel = (ElementsModel) session.get(ElementsModel.class, el_id);
        session.getTransaction().commit();
        session.close();
        return elementsModel;
    }

    public void update(ElementsModel elementsModel, int el_id) {

        Session session = factory.openSession();
        session.beginTransaction();
        elementsModel.setEl_id(el_id);
        session.update(elementsModel);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(int el_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        ElementsModel elementsModel = (ElementsModel) session.get(ElementsModel.class, el_id);
        session.delete(elementsModel);
        session.getTransaction().commit();
        session.close();
    }

}
