package Model;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Top_Sumeta
 */

public class IndexModel {

    public static SessionFactory factory;

    static {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Exception ex) {
            System.err.println(ex);
            throw new ExceptionInInitializerError(ex);
        }

    }

}
