package Model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * ตาราง re_indicators_personnel เป็นการจัดการบุคลากร ในแต่ละตัวบ่งชี
 *
 * @author top_sumeta ; email : tp_sumeta@gamil.com
 */
public class ReIndicatorsPersonnelModel extends IndexModel {

    private int ip_id;   // pk ของตารางนี้
    private int in_id;   // fk ของตารางนี้  คือรหัสตัวบ่งชี้ เชื่อมต่อกับตารางตัวบ่งชี้  sar_indicators
    private int p_id;   // fk ของตารางนี้ คือรหัสของบุคลากร เชื่อมต่อับตารางบุคลากร personnel
    private int ip_position;  // ตำแหน่งของบุคลการ (ผู้กำกับ,ผุ้จัดเก็บข้อมูล,ผู้เขียนผลการดำเนินงาน,)

    public int getIp_id() {
        return ip_id;
    }

    public void setIp_id(int ip_id) {
        this.ip_id = ip_id;
    }

    public int getIn_id() {
        return in_id;
    }

    public void setIn_id(int in_id) {
        this.in_id = in_id;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public int getIp_position() {
        return ip_position;
    }

    public void setIp_position(int ip_position) {
        this.ip_position = ip_position;
    }

    // รายชื่อของตัวบ่งชี้ทั้งหมด
    public List list() {
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria indicators = session.createCriteria(IndicatorsModel.class);
        return indicators.list();
    }

    // หาข้อมูล เมื่อกำหนด ip_id
    public List where_ip_id() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ReIndicatorsPersonnelModel WHERE ip_id=:ip_id");
        query.setParameter("ip_id", getIp_id());
        List indicators = query.list();
        session.getTransaction().commit();
        session.close();
        return indicators;
    }

    // แสดงข้อมูลเมื่อกำหนด in_id (in_id เป็นรหัสของตัวบ่งชี้)
    public List where_in_id() {
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ReIndicatorsPersonnelModel WHERE in_id=:in_id");
        query.setParameter("in_id", getIn_id());
        List list1 = query.list();
        session.getTransaction().commit();
        session.close();
        return list1;
    }

    public ReIndicatorsPersonnelModel save(ReIndicatorsPersonnelModel reIndicatorsPersonnelModel) {
        Session session = factory.openSession();
        session.beginTransaction();
        session.save(reIndicatorsPersonnelModel);
        session.getTransaction().commit();
        return reIndicatorsPersonnelModel;
    }

    public ReIndicatorsPersonnelModel find(int in_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        ReIndicatorsPersonnelModel reIndicatorsPersonnelModel = (ReIndicatorsPersonnelModel) session.get(ReIndicatorsPersonnelModel.class, in_id);
        session.getTransaction().commit();
        session.close();
        return reIndicatorsPersonnelModel;
    }

    // แก้ไขบุคลาการในตัวบ่งชี้

    public void update(ReIndicatorsPersonnelModel reIndicatorsPersonnelModel, int in_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        reIndicatorsPersonnelModel.setIp_id(ip_id);
        session.update(reIndicatorsPersonnelModel);
        session.getTransaction().commit();
    }

    // ลบบุคลากรออกจากตางราง re_indicators_personnel
    public void delete(int ip_id) {
        Session session = factory.openSession();
        session.beginTransaction();
        ReIndicatorsPersonnelModel reIndicatorsPersonnelModel = (ReIndicatorsPersonnelModel) session.get(ReIndicatorsPersonnelModel.class, ip_id);
        session.delete(reIndicatorsPersonnelModel);
        session.getTransaction().commit();
        session.close();
    }

    // ตรวจสอบสิทธิการเข้าถึง
    public List att() {
        /*
            SELECT * from re_indicators_personnel
            left join personnel
            on re_indicators_personnel.p_id = personnel.p_id
            where in_id=${in_id} 
        */
        Session session = factory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM ReIndicatorsPersonnelModel WHERE ip_id=:ip_id");
        
        query.setParameter("ip_id", getIp_id());
        List indicators = query.list();
        session.getTransaction().commit();
        session.close();
        return indicators;
    }

}
